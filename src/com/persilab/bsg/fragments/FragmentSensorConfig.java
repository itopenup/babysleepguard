package com.persilab.bsg.fragments;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.SwitchPreference;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.DatePreference;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;

import java.text.ParseException;
import java.util.*;

/**
 * Created by konstantin on 22.09.14.
 */
public class FragmentSensorConfig extends BasePreferenceFragment implements OnSocketClientListener {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.widget_sensor_preferences);
	}

	@Override
	public void onStop() {

		/**
		 * CMD_SET_DATE_TIME
		 * CMD_SET_APP_PARAMS
		 * */

		if(bound) {
			try {
				SocketDataPacket.RealDateTime dateTime = new SocketDataPacket.RealDateTime(DatePreference.formatter().parse(
					preferences.getString("pref_settings_sensor_datetime", "")
				));

				svc.send(SocketDataPacket.CMD_SET_DATE_TIME, dateTime.getBytes());
			} catch (ParseException e) {
				e.printStackTrace();
			}


			try {
				SocketDataPacket.SuParameters appParams = new SocketDataPacket.SuParameters();
				appParams.bSoundEnabled = preferences.getBoolean("pref_settings_sensor_sound_flag", false);
				appParams.nMicLevel = Byte.parseByte(
					preferences.getString("pref_settings_sensor_mic_sensitivity", "10"));
				appParams.nLedBlinkingPeriod = Byte.parseByte(
					preferences.getString("pref_settings_sensor_sensors_request_frequency", "10"));
				appParams.nNightlightDurationSec = Byte.parseByte(
					preferences.getString("pref_settings_sensor_auto_off_nightlight", "10"));
				appParams.nPowerOffTimeout = Byte.parseByte(
					preferences.getString("pref_settings_sensor_auto_off_sensor", "10"));

				svc.send(SocketDataPacket.CMD_SET_APP_PARAMS, appParams.getBytes());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		super.onStop();
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		super.onServiceConnected(name, binder);

		/**
		 * CMD_GET_DATE_TIME
		 * CMD_GET_APP_PARAMS
		 * */

		if(bound) {
			svc.addSocketClientListener(this);

			svc.send(SocketDataPacket.CMD_GET_DATE_TIME);
			svc.send(SocketDataPacket.CMD_GET_APP_PARAMS);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(bound) {
			svc.removeSocketClientListener(this);
		}
	}

	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {
		Map<String, String> values;

		switch(cmd) {
			case SocketDataPacket.CMD_GET_DATE_TIME:
				SocketDataPacket.RealDateTime dateTime = null;
				try {
					dateTime = new SocketDataPacket.RealDateTime(data);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}

				GregorianCalendar cal = new GregorianCalendar();
				--dateTime.mm;
				cal.set(dateTime.yyyy, dateTime.mm, dateTime.dd, dateTime.hh, dateTime.nn, dateTime.ss);

				values = new HashMap<String, String>();
				values.put("pref_settings_sensor_datetime",
					DatePreference.formatter().format(new Date(cal.getTimeInMillis())));
				setPreferenceValueOrRemove(values);
				break;

			case SocketDataPacket.CMD_GET_APP_PARAMS:
				SocketDataPacket.SuParameters appParams = null;
				try {
					appParams = new SocketDataPacket.SuParameters(data);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}

				preferences.edit()
					.putBoolean("pref_settings_sensor_sound_flag", appParams.bSoundEnabled)
					.apply();

				((SwitchPreference) findPreference("pref_settings_sensor_sound_flag")).setChecked(
					appParams.bSoundEnabled);

				values = new HashMap<String, String>();
				values.put("pref_settings_sensor_mic_sensitivity", String.valueOf(appParams.nMicLevel));
				values.put("pref_settings_sensor_sensors_request_frequency", String.valueOf(appParams.nLedBlinkingPeriod));
				values.put("pref_settings_sensor_auto_off_nightlight", String.valueOf(appParams.nNightlightDurationSec));
				values.put("pref_settings_sensor_auto_off_sensor", String.valueOf(appParams.nPowerOffTimeout));

				setPreferenceValueOrRemove(values);
				break;
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {

	}
}