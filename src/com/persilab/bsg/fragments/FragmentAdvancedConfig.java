package com.persilab.bsg.fragments;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by konstantin on 17.09.14.
 */
public class FragmentAdvancedConfig extends BasePreferenceFragment implements OnSocketClientListener {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*double d = 1000;

		byte[] ba = ByteBuffer.allocate(8).putDouble(d).array();
		byte[] ba1 = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putDouble(d).array();
		byte[] ba2 = ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putDouble(d).array();

		double dd = ByteBuffer.wrap(ba).getDouble();
		double dd1 = ByteBuffer.wrap(ba1).order(ByteOrder.BIG_ENDIAN).getDouble();
		double dd2 = ByteBuffer.wrap(ba2).order(ByteOrder.LITTLE_ENDIAN).getDouble();*/

		addPreferencesFromResource(R.xml.widget_advanced_preferences);
	}

	@Override
	public void onStop() {

		/**
		 * CMD_SET_DEVICE_ID
		 * CMD_SET_DSP_PARAMS
		 * */

		if(bound) {

			/*svc.send(SocketDataPacket.CMD_SET_DEVICE_ID, preferences.getString(
					"pref_settings_advanced_id", "").getBytes());*/

			try {
				SocketDataPacket.DspParameters dspParams = new SocketDataPacket.DspParameters();

				dspParams.SAMPLING_RATE = Short.parseShort(preferences.getString(
					"pref_settings_advanced_sample_rate", "0"));
				dspParams.ALARM_DELAY_ON = Short.parseShort(preferences.getString(
					"pref_settings_advanced_alarm_on_delay", "0"));
				dspParams.ALARM_DELAY_OFF = Short.parseShort(preferences.getString(
					"pref_settings_advanced_alarm_off_delay", "0"));

				try {
					String noise_threshold = preferences.getString(
						"pref_settings_advanced_noise_threshold", "0").replace(',', '.');
					String movement_threshold = preferences.getString(
						"pref_settings_advanced_movement_threshold", "0").replace(',', '.');

					dspParams.NOISE_THRESHOLD = Double.valueOf(noise_threshold);
					dspParams.MOTION_THRESHOLD = Double.valueOf(movement_threshold);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

				svc.send(SocketDataPacket.CMD_SET_DSP_PARAMS, dspParams.getBytes());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		super.onStop();
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		super.onServiceConnected(name, binder);

		/**
		 * CMD_GET_DEVICE_ID
		 * CMD_GET_DSP_PARAMS
		 * */

		if(bound) {
			svc.addSocketClientListener(this);

			svc.send(SocketDataPacket.CMD_GET_DEVICE_ID);
			svc.send(SocketDataPacket.CMD_GET_DSP_PARAMS);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(bound) {
			svc.removeSocketClientListener(this);
		}
	}

	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {
		Map<String, String> values;

		switch(cmd) {
			case SocketDataPacket.CMD_GET_DEVICE_ID:
				preferences.edit()
					.putString("pref_settings_advanced_id", new String(data))
					.apply();
				setSummaryFromValue("pref_settings_advanced_id", new String(data));
				break;

			case SocketDataPacket.CMD_GET_DSP_PARAMS:
				SocketDataPacket.DspParameters dspParams;
				try {
					dspParams = new SocketDataPacket.DspParameters(data);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}

				NumberFormat formatter = new DecimalFormat("0.#####E0");
				values = new HashMap<String, String>();
				values.put("pref_settings_advanced_sample_rate", String.valueOf(dspParams.SAMPLING_RATE));
				values.put("pref_settings_advanced_alarm_on_delay", String.valueOf(dspParams.ALARM_DELAY_ON));
				values.put("pref_settings_advanced_alarm_off_delay", String.valueOf(dspParams.ALARM_DELAY_OFF));
				values.put("pref_settings_advanced_noise_threshold", formatter.format(dspParams.NOISE_THRESHOLD));
				values.put("pref_settings_advanced_movement_threshold", formatter.format(dspParams.MOTION_THRESHOLD));

				setPreferenceValueOrRemove(values);
				break;
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {

	}
}