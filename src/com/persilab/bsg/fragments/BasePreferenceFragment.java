package com.persilab.bsg.fragments;

import android.content.*;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.*;
import android.util.Log;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.services.BinderServiceDataTransfer;
import com.persilab.bsg.services.ServiceDataTransfer;

import java.util.Map;
import java.util.Set;

/**
 * Created by Константин on 10.10.2014.
 */
public abstract class BasePreferenceFragment extends PreferenceFragment implements
	SharedPreferences.OnSharedPreferenceChangeListener,

	ServiceConnection {

	protected boolean bound = false;
	protected ServiceConnection svcConnection;
	protected Intent svcIntent;
	protected ServiceDataTransfer svc;
	protected SharedPreferences preferences;
	protected Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getPreferenceManager().setSharedPreferencesName(PrefWrap.PREFERENCES_NAME);

		preferences = PrefWrap.getPreferences(getActivity());
		svcIntent = new Intent(getActivity(), ServiceDataTransfer.class);
		context = getActivity();
	}

	@Override
	public void onStart() {
		super.onStart();

		Set<String> keys = preferences.getAll().keySet();
		for(String key : keys) {
			setSummaryFromValue(key);
		}

		preferences.registerOnSharedPreferenceChangeListener(this);

		getActivity().bindService(svcIntent, this, 0);
	}

	@Override
	public void onStop() {
		super.onStop();

		preferences.unregisterOnSharedPreferenceChangeListener(this);

		if (bound) {
			getActivity().unbindService(this);
			bound = false;
		}

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Log.d("SPCH",
			"pref: " + key
		);

		setSummaryFromValue(key);
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		svc = ((BinderServiceDataTransfer) binder).getService();
		bound = true;
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		bound = false;
		svc = null;
	}

	protected void setSummaryFromValue(String key) {
		Preference pref = findPreference(key);

		if(pref != null) {
			if (pref instanceof EditTextPreference) {
				EditTextPreference etp = (EditTextPreference) pref;
				pref.setSummary(etp.getText());
			} else if (pref instanceof ListPreference) {
				ListPreference lp = (ListPreference) pref;
				pref.setSummary(lp.getEntry());
			} else if(pref instanceof RingtonePreference) {
				Uri ringtoneUri = Uri.parse(preferences.getString(
						PrefWrap.CONFIG_KEY_APP_ALARM_TONE, ""));
				Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneUri);
				pref.setSummary(ringtone.getTitle(context));
			}
		}
	}

	protected void setSummaryFromValue(String key, String value) {
		Preference pref = findPreference(key);

		if(pref != null) {
			pref.setSummary(value);
		}
	}

	protected void setPreferenceValueOrRemove(Map<String, String> values) {
		if(values == null || values.isEmpty()) {
			return;
		}

		SharedPreferences.Editor editor = preferences.edit();

		for(String key : values.keySet()) {
			String value = values.get(key);

			if(!value.isEmpty()) {
				editor.putString(key, value);
			} else {
				editor.remove(key);
			}
		}

		editor.apply();

		for(String key : values.keySet()) {
			String value = values.get(key);
			setSummaryFromValue(key, value);
		}
	}

	protected void setPreferenceValueOrRemove(String key, String value) {
		SharedPreferences.Editor editor = preferences.edit();

		if(!value.isEmpty()) {
			editor.putString(key, value);
		} else {
			editor.remove(key);
		}

		editor.apply();
		//setSummaryFromValue(key);
	}
}
