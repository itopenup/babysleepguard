package com.persilab.bsg.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.PrefWrap;

import java.io.File;

/**
 * Created by Константин on 19.11.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class FragmentLogFilesDialog extends DialogFragment {

	public static final String ARG_PATH = "path";

	ListView list;
	String path;

	public static FragmentLogFilesDialog newInstance() {
		FragmentLogFilesDialog instance = new FragmentLogFilesDialog();
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
		path = getArguments().getString(ARG_PATH);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_logfiles_dialog, container, false);
		getDialog().setTitle(R.string.logfiles_dialog_title);

		list = (ListView) v.findViewById(R.id.list);

		File dir = new File(path);
		File files[] = dir.listFiles();
		ArrayAdapter<File> adapterFiles = new ArrayAdapter<File>(getActivity(),
			R.layout.row_logfiles_name, R.id.text, files);
		list.setAdapter(adapterFiles);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(getActivity(),
					((File)parent.getItemAtPosition(position)).getName(), Toast.LENGTH_LONG).show();
			}
		});

		v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});



		return v;
	}


}