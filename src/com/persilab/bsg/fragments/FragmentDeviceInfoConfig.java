package com.persilab.bsg.fragments;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;

/**
 * Created by konstantin on 22.09.14.
 */
public class FragmentDeviceInfoConfig extends BasePreferenceFragment implements OnSocketClientListener {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.widget_device_info_preferences);
 	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(bound) {
			svc.removeSocketClientListener(this);
		}
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		super.onServiceConnected(name, binder);

		/**
		 * CMD_GET_DEVICE_ID
		 * CMD_GET_DEVICE_NAME
		 * CMD_GET_DEVICE_CLASS
		 * CMD_GET_DEVICE_FW
		 * CMD_GET_DEVICE_HW
		 * */

		if(bound) {
			svc.addSocketClientListener(this);

			svc.send(SocketDataPacket.CMD_GET_DEVICE_ID);
			svc.send(SocketDataPacket.CMD_GET_DEVICE_NAME);
			svc.send(SocketDataPacket.CMD_GET_DEVICE_CLASS);
			svc.send(SocketDataPacket.CMD_GET_DEVICE_FW);
			svc.send(SocketDataPacket.CMD_GET_DEVICE_HW);
		}
	}

	@SuppressLint("CommitPrefEdits")
	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {
		String key = null, value = null;

		switch(cmd) {
			case SocketDataPacket.CMD_GET_DEVICE_ID:
				key = "pref_settings_device_info_id";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEVICE_NAME:
				key = "pref_settings_device_info_name";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEVICE_CLASS:
				key = "pref_settings_device_info_class";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEVICE_FW:
				key = "pref_settings_device_info_fw";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEVICE_HW:
				key = "pref_settings_device_info_hw";
				value = new String(data);
				break;
		}

		if(key != null) {
			value = SocketDataPacket.parseStringResponse(value);
			setPreferenceValueOrRemove(key, value);
			setSummaryFromValue(key, value);
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {

	}
}