package com.persilab.bsg.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.PrefWrap;

/**
 * Created by Константин on 19.11.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class FragmentResumeDialog extends DialogFragment {

	public interface OnResumeDialogActionListener {
		void onAccept(String address);
		void onCancel();
		void onDemo();
	}

	private OnResumeDialogActionListener onResumeDialogActionListener;
	private TextView tvAddress;

	public static FragmentResumeDialog newInstance(OnResumeDialogActionListener onResumeDialogActionListener) {
		FragmentResumeDialog instance = new FragmentResumeDialog();
		instance.onResumeDialogActionListener = onResumeDialogActionListener;

		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_Dialog);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_resume_dialog, container, false);
		getDialog().setTitle(R.string.resume_dialog_title);
		getDialog().setCanceledOnTouchOutside(false);

		tvAddress = (TextView) v.findViewById(R.id.address);

		String ip, port;
		StringBuilder address = new StringBuilder();

		ip = PrefWrap.getStr(getActivity(), PrefWrap.CONFIG_KEY_CONNECTION_IP, "192.168.0.101");
		port = PrefWrap.getStr(getActivity(), PrefWrap.CONFIG_KEY_CONNECTION_PORT, "59136");

		if(!ip.isEmpty()) {
			address.append(ip);
		} else {
			address.append("0.0.0.0");
		}

		if(!port.isEmpty()) {
			address.append(":");
			address.append(port);
		}

		tvAddress.setText(address);

		v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onResumeDialogActionListener.onAccept(tvAddress.getText().toString());
				dismiss();
			}
		});

		v.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onResumeDialogActionListener.onCancel();
				dismiss();
			}
		});

		v.findViewById(R.id.demo).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onResumeDialogActionListener.onDemo();
				dismiss();
			}
		});

		return v;
	}
}