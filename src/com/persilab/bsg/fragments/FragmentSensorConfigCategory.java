package com.persilab.bsg.fragments;

import android.content.ComponentName;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceFragment;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;


public class FragmentSensorConfigCategory extends PreferenceFragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.widget_sensor_preferences_category);
 	}
}