package com.persilab.bsg.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.PrefWrap;

/**
 * Created by Константин on 19.11.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class FragmentPauseDialog extends DialogFragment {

	public interface OnPauseDialogActionListener {
		void onAccept();
		void onCancel();
	}

	private OnPauseDialogActionListener onPauseDialogActionListener;

	public static FragmentPauseDialog newInstance(OnPauseDialogActionListener onResumeDialogActionListener) {
		FragmentPauseDialog instance = new FragmentPauseDialog();
		instance.onPauseDialogActionListener = onResumeDialogActionListener;

		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_Dialog);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_pause_dialog, container, false);
		getDialog().setTitle(R.string.pause_dialog_title);
		getDialog().setCanceledOnTouchOutside(false);

		v.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onPauseDialogActionListener.onAccept();
				dismiss();
			}
		});

		v.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onPauseDialogActionListener.onCancel();
				dismiss();
			}
		});

		return v;
	}
}