package com.persilab.bsg.fragments;

import android.content.ComponentName;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;


public class FragmentConnectionConfig extends BasePreferenceFragment implements OnSocketClientListener {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.widget_connection_preferences);
 	}

	@Override
	public void onStop() {


		/**
		 * CMD_SET_WIFI_MODE
		 * CMD_SET_SSID
		 * CMD_SET_SECURITY_TYPE
		 * CMD_SET_PASSWORD
		 * CMD_SET_DEFAULT_IP_ADDRESS
		 * CMD_SET_DEFAULT_IP_MASK
		 * CMD_SET_DEFAULT_IP_GATE
		 * CMD_SET_DEFAULT_IP_PORT
		 * */

		if(bound) {

			svc.send(SocketDataPacket.CMD_SET_WIFI_MODE,
				preferences.getString("pref_settings_lan_wifi_mode", "AP").getBytes());

			svc.send(SocketDataPacket.CMD_SET_SSID,
				preferences.getString("pref_settings_lan_ssid", "").getBytes());

			svc.send(SocketDataPacket.CMD_SET_SECURITY_TYPE,
				preferences.getString("pref_settings_lan_encrypt_type", "OPEN").getBytes());

			svc.send(SocketDataPacket.CMD_SET_PASSWORD,
				preferences.getString("pref_settings_lan_encrypt_key", "").getBytes());

			svc.send(SocketDataPacket.CMD_SET_DEFAULT_IP_ADDRESS,
				preferences.getString("pref_settings_lan_default_ip", "").getBytes());

			svc.send(SocketDataPacket.CMD_SET_DEFAULT_IP_MASK,
				preferences.getString("pref_settings_lan_default_ip_mask", "255.255.255.0").getBytes());

			svc.send(SocketDataPacket.CMD_SET_DEFAULT_IP_GATE,
				preferences.getString("pref_settings_lan_default_getaway", "0.0.0.0").getBytes());

			svc.send(SocketDataPacket.CMD_SET_DEFAULT_IP_PORT,
				preferences.getString("pref_settings_lan_default_port", "0").getBytes());
		}

		super.onStop();
 	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences pref, String key) {
		super.onSharedPreferenceChanged(pref, key);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(bound) {
			svc.removeSocketClientListener(this);
		}
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		super.onServiceConnected(name, binder);

		/**
		 * CMD_GET_MAC_ADDRESS
		 * CMD_GET_WIFI_MODE
		 * CMD_GET_SSID
		 * CMD_GET_SECURITY_TYPE
		 * CMD_GET_PASSWORD
		 * CMD_GET_DEFAULT_IP_ADDRESS
		 * CMD_GET_DEFAULT_IP_MASK
		 * CMD_GET_DEFAULT_IP_GATE
		 * CMD_GET_DEFAULT_IP_PORT
		 * */

		if(bound) {
			svc.addSocketClientListener(this);

			svc.send(SocketDataPacket.CMD_GET_MAC_ADDRESS);
			svc.send(SocketDataPacket.CMD_GET_WIFI_MODE);
			svc.send(SocketDataPacket.CMD_GET_SSID);
			svc.send(SocketDataPacket.CMD_GET_SECURITY_TYPE);
			svc.send(SocketDataPacket.CMD_GET_PASSWORD);
			svc.send(SocketDataPacket.CMD_GET_DEFAULT_IP_ADDRESS);
			svc.send(SocketDataPacket.CMD_GET_DEFAULT_IP_MASK);
			svc.send(SocketDataPacket.CMD_GET_DEFAULT_IP_GATE);
			svc.send(SocketDataPacket.CMD_GET_DEFAULT_IP_PORT);
		}
	}

	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {
		String key = null, value = null;

		switch(cmd) {
			case SocketDataPacket.CMD_GET_MAC_ADDRESS:
				key = "pref_settings_lan_mac";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_WIFI_MODE:
				key = "pref_settings_lan_wifi_mode";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_SSID:
				key = "pref_settings_lan_ssid";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_SECURITY_TYPE:
				key = "pref_settings_lan_encrypt_type";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_PASSWORD:
				key = "pref_settings_lan_encrypt_key";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEFAULT_IP_ADDRESS:
				key = "pref_settings_lan_default_ip";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEFAULT_IP_MASK:
				key = "pref_settings_lan_default_ip_mask";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEFAULT_IP_GATE:
				key = "pref_settings_lan_default_getaway";
				value = new String(data);
				break;

			case SocketDataPacket.CMD_GET_DEFAULT_IP_PORT:
				key = "pref_settings_lan_default_port";
				value = new String(data);
				break;
		}

		if(key != null) {
			//SharedPreferences.Editor editor = preferences.edit();
			value = SocketDataPacket.parseStringResponse(value);
			setPreferenceValueOrRemove(key, value);
			setSummaryFromValue(key, value);

			/*if(!value.isEmpty()) {
				editor.putString(key, value).apply();
			} else {
				editor.remove(key).apply();
			}*/
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {

	}
}