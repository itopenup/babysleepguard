package com.persilab.bsg.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.Preference;
import android.preference.RingtonePreference;
import android.util.Log;
import android.widget.Toast;
import com.persilab.bsg.R;
import com.persilab.bsg.activities.ActivityConfig;
import com.persilab.bsg.lib.ListPreference;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;
import com.persilab.bsg.tasks.TaskLogFile;

import java.io.File;
import java.util.Random;


public class FragmentAppConfig extends BasePreferenceFragment implements
	OnSocketClientListener,
	Preference.OnPreferenceClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.widget_preferences_app);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if(preference.getKey().equals(PrefWrap.CONFIG_KEY_APP_SHOW_LOG)) {
			File dir = TaskLogFile.getLogsDir(getActivity());
			if(dir != null && dir.exists()) {
				//Toast.makeText(getActivity(), dir.getPath(), Toast.LENGTH_LONG).show();
				FragmentLogFilesDialog fragment = new FragmentLogFilesDialog();
				Bundle args = new Bundle();
				args.putString(FragmentLogFilesDialog.ARG_PATH, dir.getPath());
				fragment.setArguments(args);

				fragment.show(getFragmentManager(), "LogFilesDialog");
			}

			return true;
		}

		return false;
	}

	@Override
	public void onStart() {
		super.onStart();

		Preference prefShowLog = findPreference(PrefWrap.CONFIG_KEY_APP_SHOW_LOG);
		if(prefShowLog != null) {
			prefShowLog.setOnPreferenceClickListener(this);
		}

		Bundle args = getArguments();

		String openPrefKey = args.getString(ActivityConfig.EXTRA_CONFIG_KEY, "");
		if(!openPrefKey.isEmpty()) {
			Preference pref = findPreference(openPrefKey);

			if(pref != null) {
				if(pref instanceof ListPreference) {
					((ListPreference)pref).show();
				}
			}
		}

		RingtonePreference ringPref = (RingtonePreference) findPreference(PrefWrap.CONFIG_KEY_APP_ALARM_TONE);
		if(ringPref != null) {
			Uri ringtoneUri = Uri.parse(preferences.getString(PrefWrap.CONFIG_KEY_APP_ALARM_TONE, ""));
			Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneUri);
			ringPref.setSummary(ringtone.getTitle(context));
		}
	}

	@Override
	public void onStop() {
		super.onStop();

		String value = PrefWrap.getStr(getActivity(), PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR);
		SocketDataPacket.SuCmd suCmd = new SocketDataPacket.SuCmd();

		if (PrefWrap.NIGHTLIGHT_COLOR_RANDOM.equals(value)) {
			Random rnd = new Random();
			int i = rnd.nextInt(SocketDataPacket.NightlightColor.values().length);
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.values()[i];
			//suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_BLUE.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_BLUE;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_CYAN.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_CYAN;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_GREEN.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_GREEN;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_MAGENTA.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_MAGENTA;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_RED.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_RED;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_WHITE.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_WHITE;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_YELLOW.equals(value)) {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_YELLOW;
		}
		else {
			suCmd.nNightlightColor = SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF;
		}

		Log.d("BSG", "CMD_SET_NIGHTLIGHT request: " + String.valueOf(suCmd.nNightlightColor));

		suCmd.nNightlightDurationSec = (short)PrefWrap.getInt(getActivity(),
				PrefWrap.CONFIG_KEY_SENSOR_NIGHTLIGHT_OFF);
		svc.send(SocketDataPacket.CMD_SET_NIGHTLIGHT, suCmd.getBytes());
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		super.onServiceConnected(name, binder);

		if(bound) {
			svc.addSocketClientListener(this);
			svc.send(SocketDataPacket.CMD_GET_DATA);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(bound) {
			svc.removeSocketClientListener(this);
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		super.onSharedPreferenceChanged(sharedPreferences, key);
	}

	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {

		switch (cmd) {
			case SocketDataPacket.CMD_SET_NIGHTLIGHT:
			case SocketDataPacket.CMD_GET_DATA:
				SocketDataPacket.SuData suData;
				try {
					suData = new SocketDataPacket.SuData(data);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}

				Log.d("BSG", "CMD_SET_NIGHTLIGHT response: " + String.valueOf(suData.nNightlightColor));

				String value;

				switch (suData.nNightlightColor) {
					case NIGHTLIGHT_BLUE:
						value = "NIGHTLIGHT_BLUE";
						break;

					case NIGHTLIGHT_CYAN:
						value = "NIGHTLIGHT_CYAN";
						break;

					case NIGHTLIGHT_GREEN:
						value = "NIGHTLIGHT_GREEN";
						break;

					case NIGHTLIGHT_MAGENTA:
						value = "NIGHTLIGHT_MAGENTA";
						break;

					case NIGHTLIGHT_RED:
						value = "NIGHTLIGHT_RED";
						break;

					case NIGHTLIGHT_WHITE:
						value = "NIGHTLIGHT_WHITE";
						break;

					case NIGHTLIGHT_YELLOW:
						value = "NIGHTLIGHT_YELLOW";
						break;

					default:
						value = "NIGHTLIGHT_OFF";
				}

				setPreferenceValueOrRemove(PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR, value);
				setSummaryFromValue(PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR);
				break;
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {

	}
}