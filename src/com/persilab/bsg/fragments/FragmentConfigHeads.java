package com.persilab.bsg.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;

/**
 * Created by konstantin on 30.10.14.
 */
public class FragmentConfigHeads extends BasePreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.widget_preferences_heads);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		super.onSharedPreferenceChanged(sharedPreferences, key);

		if(PrefWrap.CONFIG_KEY_APP_DEMO_MODE.equals(key)) {
			if(bound) {
				svc.send(SocketDataPacket.CMD_GET_DATA);
			}
		}
	}
}