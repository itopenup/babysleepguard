package com.persilab.bsg.services;

import android.os.Binder;

/**
 * Created by Константин on 10.10.2014.
 */
public class BinderServiceDataTransfer extends Binder {

	private ServiceDataTransfer service;

	public BinderServiceDataTransfer(ServiceDataTransfer service) {
		super();
		this.service = service;
	}

	public ServiceDataTransfer getService() {
		return service;
	}
}
