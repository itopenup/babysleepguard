package com.persilab.bsg.services;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.*;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import com.persilab.bsg.R;
import com.persilab.bsg.activities.ActivityWidget;
import com.persilab.bsg.lib.Logger;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.OnSocketClientListener;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataTransfer;
import com.persilab.bsg.lib.state.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class ServiceDataTransfer extends Service implements OnSocketClientListener {

	private boolean isPaused, isDemo;
	private BinderServiceDataTransfer binder;
	private SocketDataTransfer soDataTransfer;
	private short deviceID;
	//private SharedPreferences preferences;
	private ArrayList<OnSocketClientListener> socketClientListeners;
	private ArrayList<SocketDataPacket> pendingPackets;
	private State deviceState;
	private PendingIntent piDeviceData;
	private AlarmManager alarmManager;
	private long deviceDataAlarmInterval = 5000, lastNotificationTime = 0;
	//private long lastSoRequestTime = 0;
	private BroadcastReceiver brcWifiConnectionReceiver;
	private Logger logger;
	private boolean enableLogger;

	private static final int NOTIFICATION_ALERT_ID = 1;

	private static final String ACTION_DEVICE_DATA_ALARM = "BSG.ServiceDataTransfer.DeviceDataAlarm";
	public static final String ACTION_DEVICE_CURRENT_STATE = "BSG.ServiceDataTransfer.DeviceCurrentState";
	public static final String ACTION_WIDGET_PAUSE = "BSG.ServiceDataTransfer.WidgetPause";
	public static final String ACTION_WIDGET_RESUME = "BSG.ServiceDataTransfer.WidgetResume";
	public static final String ACTION_DEVICE_DATA = "BSG.ServiceDataTransfer.DeviceData";
	public static final String ACTION_COMMAND = "BSG.ServiceDataTransfer.Cmd";

	public static final String EXTRA_STATE = "state";
	public static final String EXTRA_PAUSED = "paused";
	public static final String EXTRA_CMD = "cmd";
	public static final String EXTRA_DATA = "data";
	public static final String EXTRA_IS_DEMO = "demo";

	@Override
	public void onCreate() {
		super.onCreate();

		binder = new BinderServiceDataTransfer(this);
		//preferences = PrefWrap.getPreferences(this);
		socketClientListeners = new ArrayList<OnSocketClientListener>();
		pendingPackets = new ArrayList<SocketDataPacket>();
		isPaused = true;
		enableLogger = PrefWrap.getBool(this, PrefWrap.CONFIG_KEY_APP_LOG_ENABLED);
		isDemo = PrefWrap.getBool(this, PrefWrap.CONFIG_KEY_APP_DEMO_MODE);
		logger = Logger.getInstance(this);
		logger.setEnabled(PrefWrap.getBool(this,
				PrefWrap.CONFIG_KEY_APP_LOG_ENABLED));

		if(enableLogger) {
			logger.log("Create service");
		}

		_clearDeviceState();
		//reconnect();

		Intent intent = new Intent(this, ServiceDataTransfer.class);
		intent.setAction(ACTION_DEVICE_DATA_ALARM);
		piDeviceData = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		/*alarmManager.setRepeating(AlarmManager.RTC,
				System.currentTimeMillis(), deviceDataAlarmInterval, piDeviceData);*/


		brcWifiConnectionReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				ConnectivityManager conn =  (ConnectivityManager)
						context.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo networkInfo = conn.getActiveNetworkInfo();

				//Log.d("BSG", "Connection info: " + intent.getAction());

				if(networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
					if(
						!isPaused
						&& !isDemo
						&& (
							soDataTransfer == null
							|| (
								!soDataTransfer.isConnected()
								&& !soDataTransfer.isConnecting()
							)
						)
					) {
						reconnect();
					}
				}
			}
		};

		registerReceiver(brcWifiConnectionReceiver,
				new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		logger.setEnabled(PrefWrap.getBool(this,
				PrefWrap.CONFIG_KEY_APP_LOG_ENABLED));
		isDemo = PrefWrap.getBool(this,
				PrefWrap.CONFIG_KEY_APP_DEMO_MODE);

		try {
			String action = intent.getAction();
			Log.d("BSG", "Service started with action: " + action);

			if(action != null) {
				logger.log("Service started with action: ", action);
			} else {
				logger.log("Service started without action");
			}

			if(!isPaused && ACTION_DEVICE_DATA_ALARM.equals(action)) {
				if(isDemo) {
					_generateRandomState();
					_broadcastDeviceState();
				} else {
					send(SocketDataPacket.CMD_GET_DATA);
				}

				int interval = PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_APP_REQUEST_INTERVAL) * 1000;
				if(interval > 1000 && interval != deviceDataAlarmInterval) {
					deviceDataAlarmInterval = interval;
					alarmManager.setRepeating(AlarmManager.RTC,
							System.currentTimeMillis(), deviceDataAlarmInterval, piDeviceData);
				}
			}
			else if(ACTION_WIDGET_PAUSE.equals(action)) {
				isPaused = true;
				alarmManager.cancel(piDeviceData);
				_broadcastDeviceState();
				_disconnect();
			}
			else if(ACTION_WIDGET_RESUME.equals(action)) {
				isPaused = false;

				_clearDeviceState();

				if(!isDemo) {
					reconnect();
				}

				alarmManager.setRepeating(AlarmManager.RTC,
					System.currentTimeMillis(), deviceDataAlarmInterval, piDeviceData);

				try {
					//Immediately requesting device state
					piDeviceData.send();
				} catch (PendingIntent.CanceledException e) {
					e.printStackTrace();
				}

				_broadcastDeviceState();
			}
			else if(ACTION_DEVICE_CURRENT_STATE.equals(action)) {
				_broadcastDeviceState();
			}

			else if(ACTION_COMMAND.equals(action)) {
				byte cmd = intent.getByteExtra(EXTRA_CMD, (byte)0);
				byte[] data = intent.getByteArrayExtra(EXTRA_DATA);

				if(cmd > 0) {
					send(cmd, data);
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			Log.d("BSG", "OnStartCommand Service error: " + e.getMessage());
		}

		return START_STICKY;
		//return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		alarmManager.cancel(piDeviceData);
		socketClientListeners.clear();
		soDataTransfer.interrupt();
		unregisterReceiver(brcWifiConnectionReceiver);
	}

	public void send(byte cmd) {
		send(cmd, null);
	}

	public void send(byte cmd, byte[] data) {
		SocketDataPacket packet = new SocketDataPacket();
		packet.setCmd(cmd);

		if(data != null) {
			packet.setData(data);
		}

		if(
			soDataTransfer == null || (!soDataTransfer.isConnected() && !soDataTransfer.isConnecting())
		) {
			pendingPackets.add(packet);
			reconnect();
		} else {
			if(deviceID > 0) {
				if(pendingPackets.size() > 0) {
					for(SocketDataPacket p : pendingPackets) {
						p.setDeviceAddress(deviceID);
						soDataTransfer.pushCmd(p);
						logger.log("Command sent", String.valueOf(p.getCmd()));
					}

					pendingPackets.clear();
				}

				packet.setDeviceAddress(deviceID);
				soDataTransfer.pushCmd(packet);
				logger.log("Command sent", String.valueOf(cmd));
			} else {
				pendingPackets.add(packet);
				soDataTransfer.pushCmd(new SocketDataPacket(
					SocketDataPacket.CMD_GET_DEVICE_ID));
			}
		}
	}

	private void _disconnect() {
		//pendingPackets.clear(); //TODO: ???

		if(soDataTransfer != null) {
			soDataTransfer.interrupt();
		}
	}

	public void reconnect() {
		if(enableLogger) {
			logger.log("Try (re)connect to socket");
		}

		_disconnect();

		deviceID = 0;
		deviceState = new StateConnecting();
		deviceState.setTemperature(0);
		deviceState.setHumidity(0);
		deviceState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
		deviceState.setBat(0);

		_broadcastDeviceState();

		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if(networkInfo == null || !networkInfo.isConnected()) {
			if(enableLogger) {
				logger.log("(re)connect to socket failed [no wi-fi connection]");
			}

			return;
		}

		String ip = PrefWrap.getStr(this, PrefWrap.CONFIG_KEY_CONNECTION_IP);
		int port = PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_CONNECTION_PORT);

		if(ip.isEmpty() || port <= 0) {
			return;
		}

		if(soDataTransfer == null) {
			soDataTransfer = new SocketDataTransfer(ip, port, this);
		} else {
			soDataTransfer.reconnect(ip, port);
		}

		soDataTransfer.pushCmd(new SocketDataPacket(SocketDataPacket.CMD_GET_DEVICE_ID));
	}

	public void addSocketClientListener(OnSocketClientListener listener) {
		socketClientListeners.add(listener);
	}

	public boolean removeSocketClientListener(OnSocketClientListener listener) {
		return socketClientListeners.remove(listener);
	}

	@Override
	public void onSocketClientResponse(byte cmd, byte[] data) {

		logger.log("Response from socket for command", String.valueOf(cmd));

		if(cmd == SocketDataPacket.CMD_GET_DEVICE_ID) {
			Log.d("BSG", "Service got response for CMD = CMD_GET_DEVICE_ID");
			try {
				String deviceIdStr = SocketDataPacket.parseStringResponse(new String(data));
				deviceID = Short.parseShort(deviceIdStr.substring(4,9));
			} catch (NumberFormatException e) {
				deviceID = 0;
			} catch (StringIndexOutOfBoundsException e) {
				deviceID = 0;
			}
		}
		else if(cmd == SocketDataPacket.CMD_GET_DATA || cmd == SocketDataPacket.CMD_SET_NIGHTLIGHT) {
			Log.d("BSG", "Service got response for CMD = CMD_GET_DATA");

			State state = null;
			HashMap<String, Property> jump = null;

			SocketDataPacket.SuData suData = null;
			try {
				suData = new SocketDataPacket.SuData(data);
			} catch (Exception e) {
				e.printStackTrace();

				_clearDeviceState();
				_broadcastDeviceState();
				return;
			}

			if (!suData.bPresence) {
				state = new StateAlarm();
				((StateAlarm) state).setNobody(suData.nPresenceDurationSec);
			} else if (suData.nRespirationRate <= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_BREATH_LOW)) {
				state = new StateAlarm();
				((StateAlarm) state).setRareBreath();
			} else if (suData.nRespirationRate >= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_BREATH_HIGH)) {
				state = new StateAlarm();
				((StateAlarm) state).setFrequentBreath();
			} else if (suData.nPulseRate <= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_PULSE_LOW)) {
				state = new StateAlarm();
				((StateAlarm) state).setRarePulse();
			} else if (suData.nPulseRate >= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_PULSE_HIGH)) {
				state = new StateAlarm();
				((StateAlarm) state).setFrequentPulse();
			} else if (suData.bSound && suData.nSoundDurationSec >= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_NOISE_REACT_TIME)) {
				state = new StateVoice();
			} else if (suData.bMotion && suData.nMotionDurationSec >= PrefWrap.getInt(this, PrefWrap.CONFIG_KEY_ADVANCED_MOTION_REACT_TIME)) {
				state = new StateAwake();
				((StateAwake) state).setTime(suData.nMotionDurationSec);
			} else if (suData.bMotion) {
				//TODO: check this; do we have a `motion only` state?
				state = new StateAwake();
				((StateAwake) state).setTime(0);
			} else if (suData.nBatteryCapacity < 10 && suData.nChargingStatus == 0) {
				state = new StateLowBat();
			} else if (suData.bPresence) {
				state = new StateSleep();
				((StateSleep) state).setTime(suData.nPresenceDurationSec);
				((StateSleep) state).setBreath(suData.nRespirationRate);
				((StateSleep) state).setMotion(false);
				((StateSleep) state).setPulse(suData.nPulseRate);
			}

			if (state != null) {
				if(state.type() != deviceState.type()) {
					lastNotificationTime = 0;
				}

				jump = deviceState.jump(state);
				deviceState = state;

				deviceState.setBat(suData.nBatteryCapacity);
				deviceState.setLampState(suData.nNightlightColor);
				deviceState.setHumidity(suData.nHumidity);
				deviceState.setTemperature(suData.nTemperature);

				/*if (suData.nChargingStatus == 0) {
					if(suData.nBatteryCapacity < 5) {
						deviceState.setBatState(State.BATTERY_STATE_EMPTY);
					} else if(suData.nBatteryCapacity < 10) {
						deviceState.setBatState(State.BATTERY_STATE_LOW);
					} else {
						deviceState.setBatState(State.BATTERY_STATE_FULL);
					}
				} else {
					deviceState.setBatState(State.BATTERY_STATE_CHARGE);
				}*/

				if(suData.nChargingStatus == 1) {
					deviceState.setBatState(State.BATTERY_STATE_CHARGE);
				} else if(suData.nBatteryCapacity <= 1) {
					deviceState.setBatState(State.BATTERY_STATE_EMPTY);
				} else if(suData.nBatteryCapacity <= 5) {
					deviceState.setBatState(State.BATTERY_STATE_LOW);
				} else {
					deviceState.setBatState(State.BATTERY_STATE_FULL);
				}

				if(jump != null) {
					Property propAlert = jump.get(State.PROP_ALERT);

					long time = System.currentTimeMillis();

					if(!isPaused && propAlert.getValue()) {
						if(lastNotificationTime == 0 || (time - lastNotificationTime)/1000 > PrefWrap.getInt(this,
							PrefWrap.CONFIG_KEY_APP_ALARM_PAUSE_TIME)) {

							Resources res = this.getResources();
							String alarmTone = PrefWrap.getStr(this,
								PrefWrap.CONFIG_KEY_APP_ALARM_TONE);
							String alarmType = PrefWrap.getStr(this,
								PrefWrap.CONFIG_KEY_APP_ALARM_TYPE,
								PrefWrap.ALARM_TYPE_SOUND_VIBRATE);

							lastNotificationTime = time;

							NotificationCompat.Builder ntfBuilder =
								new NotificationCompat.Builder(this)
									.setSmallIcon(R.drawable.ic_stat_notif_face)
									.setContentTitle(getString(R.string.w_notification_title))
									.setTicker(getString(R.string.w_notification_title))
									.setContentText(deviceState.getDescription(res))
									.setPriority(NotificationCompat.PRIORITY_HIGH)
									.setAutoCancel(true)
									.setDefaults(0);

							if(
								!alarmTone.isEmpty()
									&& (PrefWrap.ALARM_TYPE_SOUND.equals(alarmType) || PrefWrap.ALARM_TYPE_SOUND_VIBRATE.equals(alarmType))
								) {
								Uri toneUri = Uri.parse(alarmTone);
								ntfBuilder.setSound(toneUri);
							}

							if(
								PrefWrap.ALARM_TYPE_VIBRATE.equals(alarmType)
									|| PrefWrap.ALARM_TYPE_SOUND_VIBRATE.equals(alarmType)
								) {
								ntfBuilder.setVibrate(new long[] {0, 100, 50, 100, 50, 100});
							}

							Intent resultIntent = new Intent(this, ActivityWidget.class);

							TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
							stackBuilder.addParentStack(ActivityWidget.class);
							stackBuilder.addNextIntent(resultIntent);

							PendingIntent resultPendingIntent =
								stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
							ntfBuilder.setContentIntent(resultPendingIntent);

							NotificationManager notificationManager =
								(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
							notificationManager.notify(NOTIFICATION_ALERT_ID, ntfBuilder.build());
						}
					} else {
						NotificationManager notificationManager =
							(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.cancel(NOTIFICATION_ALERT_ID);
					}
				}
			}

			_broadcastDeviceState();
		}

		for(OnSocketClientListener listener : socketClientListeners) {
			listener.onSocketClientResponse(cmd, data);
		}
	}

	@Override
	public void onSocketClientError(byte cmd) {
		for(OnSocketClientListener listener : socketClientListeners) {
			listener.onSocketClientError(cmd);
		}

		if(enableLogger) {
			logger.log("Socket error for command", String.valueOf(cmd));
		}

		if(soDataTransfer == null || !soDataTransfer.isConnecting()) {
			reconnect();
		}
	}

	private void _clearDeviceState() {
		deviceID = 0;
		pendingPackets.clear();

		deviceState = new StateNoConnection();
		deviceState.setTemperature(0);
		deviceState.setHumidity(0);
		deviceState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
		deviceState.setBat(0);
	}

	private void _broadcastDeviceState() {
		Intent bcIntent = new Intent(ACTION_DEVICE_DATA)
			.putExtra(EXTRA_STATE, deviceState);

		bcIntent.putExtra(EXTRA_PAUSED, isPaused);
		bcIntent.putExtra(EXTRA_IS_DEMO, PrefWrap.getBool(this,
			PrefWrap.CONFIG_KEY_APP_DEMO_MODE));

		sendBroadcast(bcIntent);
	}

	private void _generateRandomState() {
		Random rnd = new Random(System.currentTimeMillis());
		int rStateId = rnd.nextInt(9); //Different states count + 1
		long rTime = rnd.nextInt(100000);
		int rBreath = rnd.nextInt(44) + 14;
		int rPulse = rnd.nextInt(44) + 74;
		boolean rMotion = rnd.nextBoolean();
		int rTemp = rnd.nextInt(41);
		int rHumidity = rnd.nextInt(71) + 10;
		int rBatState = rnd.nextInt(2);
		int rBat = rBatState == 0 ? rnd.nextInt(101) : 3;

		SocketDataPacket.NightlightColor rLamp;
		State state;

		SocketDataPacket.NightlightColor[] nlValues = SocketDataPacket.NightlightColor.values();
		rLamp = nlValues[rnd.nextInt(nlValues.length)];

		if(deviceState == null) {
			deviceState = new StateNoConnection();
			deviceState.setTemperature(0);
			deviceState.setHumidity(0);
			deviceState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
			deviceState.setBat(0);
		}

		switch (rStateId) {
			case 0:
				state = new StateAlarm();
				((StateAlarm) state).setNobody(rTime);
				break;

			case 1:
				state = new StateAlarm();
				((StateAlarm) state).setRareBreath();
				break;

			case 2:
				state = new StateAlarm();
				((StateAlarm) state).setFrequentBreath();
				break;

			case 3:
				state = new StateAlarm();
				((StateAlarm) state).setRarePulse();
				break;

			case 4:
				state = new StateAlarm();
				((StateAlarm) state).setFrequentPulse();
				break;

			case 5:
				state = new StateVoice();
				break;

			case 6:
				state = new StateAwake();
				((StateAwake) state).setTime(rTime);
				break;
			case 7:
				state = new StateLowBat();
				break;

			case 8:
				state = new StateSleep();
				((StateSleep) state).setTime(rTime);
				((StateSleep) state).setBreath(rBreath);
				((StateSleep) state).setMotion(rMotion);
				((StateSleep) state).setPulse(rPulse);
				break;

			default:
				state = new StateAwake();
				((StateAwake) state).setTime(rTime);
		}

		HashMap<String, Property> jump = deviceState.jump(state);
		deviceState = state;

		deviceState.setBat(rBat);
		deviceState.setBatState(rBatState == 0 ? State.BATTERY_STATE_CHARGE : State.BATTERY_STATE_LOW);
		deviceState.setLampState(rLamp);
		deviceState.setHumidity(rHumidity);
		deviceState.setTemperature(rTemp);
	}
}
