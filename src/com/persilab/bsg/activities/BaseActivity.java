package com.persilab.bsg.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.persilab.bsg.lib.PrefWrap;

import java.util.Locale;

/**
 * Created by konstantin on 24.11.14.
 */
public abstract class BaseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Resources res = getResources();
		// Change locale settings in the app.
		DisplayMetrics dm = res.getDisplayMetrics();
		android.content.res.Configuration conf = res.getConfiguration();

		String langCode = PrefWrap.getStr(this, PrefWrap.CONFIG_KEY_APP_LANG, "");
		if(!langCode.isEmpty() && !langCode.equals(conf.locale.getLanguage())) {
			conf.locale = new Locale(langCode.toLowerCase());
			res.updateConfiguration(conf, dm);
		}
	}
}
