package com.persilab.bsg.activities;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.*;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.persilab.bsg.R;
import com.persilab.bsg.fragments.FragmentResumeDialog;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.state.State;
import com.persilab.bsg.services.ServiceDataTransfer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Константин on 26.10.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class ActivityWidgetPaused extends BaseActivity {

	public static final String ACTION_RESUME_DIALOG = "ActivityWidgetPaused.ResumeDialog";


	BroadcastReceiver onDeviceDataReceiver;
	boolean preventReceiving = false;
	SharedPreferences preferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_widget_paused);

		/*byte[] c = new byte[] {20, 0, 15, 0, 15, 0, 0, 0, 0, 0, 0, 0, -128, 79, 2, 65, 0, 0, 0, 0, 101, -51, -51, 65};

		double d = 0;
		NumberFormat formatter = new DecimalFormat("0.#####E0");
		try {
			d = Double.valueOf("1.5E5");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		byte[] arr = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
			.putDouble(d).array();

		//byte[] b = new byte[] {0, 0, 0, 0, -128, 79, 2, 65};
		ByteBuffer buf = ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN);


		Log.d("DOUBLE", formatter.format(buf.asDoubleBuffer().get()) );*/


		preferences = PrefWrap.getPreferences(this);

		PreferenceManager.setDefaultValues(this, PrefWrap.PREFERENCES_NAME, Context.MODE_PRIVATE,
				R.xml.widget_preferences_app, true);
		PreferenceManager.setDefaultValues(this, PrefWrap.PREFERENCES_NAME, Context.MODE_PRIVATE,
				R.xml.widget_advanced_preferences, true);
		PreferenceManager.setDefaultValues(this, PrefWrap.PREFERENCES_NAME, Context.MODE_PRIVATE,
				R.xml.widget_lan_preferences, true);
		PreferenceManager.setDefaultValues(this, PrefWrap.PREFERENCES_NAME, Context.MODE_PRIVATE,
				R.xml.widget_sensor_preferences, true);

		onDeviceDataReceiver = _initDeviceStateReceiver();

		findViewById(R.id.turn_on).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//v.setEnabled(false);
				preventReceiving = true;
				_showResumeDialog();
			}
		});

		findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent infoIntent = new Intent(ActivityWidgetPaused.this, ActivityConfig.class);
				infoIntent.setAction(ActivityConfig.ACTION_FRAGMENT_ABOUT);
				startActivity(infoIntent);
			}
		});

		if (!preferences.getBoolean("IS_ICON_CREATED", false)) {
			_addShortcut();
			preferences.edit()
				.putBoolean("IS_ICON_CREATED", true)
				.apply();
		}

		if(ACTION_RESUME_DIALOG.equals(getIntent().getAction())) {
			preventReceiving = true;
			_showResumeDialog();
		}

		int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		Intent resultIntent = new Intent();
		resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
		setResult(RESULT_OK, resultIntent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(onDeviceDataReceiver,
				new IntentFilter(ServiceDataTransfer.ACTION_DEVICE_DATA));

		Intent dvcIntent = new Intent(this, ServiceDataTransfer.class);
		dvcIntent.setAction(ServiceDataTransfer.ACTION_DEVICE_CURRENT_STATE);
		startService(dvcIntent);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(onDeviceDataReceiver);
	}

	private void _showResumeDialog() {
		FragmentResumeDialog resumeDialog = FragmentResumeDialog.newInstance(
			new FragmentResumeDialog.OnResumeDialogActionListener() {

			@Override
			public void onAccept(String address) {
				String ip, port;
				Pattern addrPattern = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(:(\\d+))?");
				Matcher matcher = addrPattern.matcher(address);

				if (matcher.matches()) {
					ip = matcher.group(1);
					port = matcher.group(3);
				} else {
					Toast.makeText(getApplicationContext(),
							R.string.resume_dialog_toast_wrong_device_address, Toast.LENGTH_SHORT)
						.show();
					preventReceiving = false;
					return;
				}

				//preventReceiving = true;

				SharedPreferences.Editor preferenceEditor = preferences.edit();
				preferenceEditor.putBoolean(PrefWrap.CONFIG_KEY_APP_DEMO_MODE, false);
				preferenceEditor.putString(PrefWrap.CONFIG_KEY_CONNECTION_IP, ip);
				preferenceEditor.putString(PrefWrap.CONFIG_KEY_CONNECTION_PORT, port);
				preferenceEditor.apply();

				Intent resumeIntent = new Intent(ActivityWidgetPaused.this, ServiceDataTransfer.class);
				resumeIntent.setAction(ServiceDataTransfer.ACTION_WIDGET_RESUME);
				startService(resumeIntent);

				startActivity(new Intent(ActivityWidgetPaused.this, ActivityWidget.class));
				finish();
			}

			@Override
			public void onCancel() {
				preventReceiving = false;
			}

			@Override
			public void onDemo() {
				preferences.edit()
					.putBoolean(PrefWrap.CONFIG_KEY_APP_DEMO_MODE, true)
					.apply();

				Intent resumeIntent = new Intent(ActivityWidgetPaused.this, ServiceDataTransfer.class);
				resumeIntent.setAction(ServiceDataTransfer.ACTION_WIDGET_RESUME);
				startService(resumeIntent);

				startActivity(new Intent(ActivityWidgetPaused.this, ActivityWidget.class));
				finish(); //finish current activity ActivityWidgetPaused
			}
		});

		resumeDialog.show(getFragmentManager(), "ResumeDialog");
	}

	private BroadcastReceiver _initDeviceStateReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();

				if (ServiceDataTransfer.ACTION_DEVICE_DATA.equals(action)) {
					if(
						intent.hasExtra(ServiceDataTransfer.EXTRA_PAUSED)
						&& !intent.getBooleanExtra(ServiceDataTransfer.EXTRA_PAUSED, true)
						&& !preventReceiving
					) {
						preventReceiving = true;
						startActivity(new Intent(ActivityWidgetPaused.this, ActivityWidget.class));
						finish();
					}
				}
			}
		};
	}

	private void _addShortcut() {
		Intent shortcutIntent = new Intent(getApplicationContext(), ActivityWidgetPaused.class);
		shortcutIntent.setAction(Intent.ACTION_MAIN);
		shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);

		Intent addIntent = new Intent();
		addIntent
			.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent)
			.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name))
			.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
				Intent.ShortcutIconResource.fromContext(getApplicationContext(),
					R.drawable.app_icon))
			.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		getApplicationContext().sendBroadcast(addIntent);
	}
}