package com.persilab.bsg.activities;

import android.app.*;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.persilab.bsg.R;
import com.persilab.bsg.fragments.*;
import com.persilab.bsg.services.ServiceDataTransfer;

/**
 * Created by konstantin on 16.09.14.
 */
public class ActivityConfig extends BaseActivity {

	final String LOG_TAG = "myLogs";

	public final static String ACTION_PREFERENCE_FRAGMENT_APP_CONFIG = "FragmentAppConfig";
	public final static String ACTION_PREFERENCE_FRAGMENT_LAN_CONFIG = "FragmentLanConfig";
	public final static String ACTION_PREFERENCE_FRAGMENT_SENSOR_CONFIG = "FragmentSensorConfig";
	public final static String ACTION_PREFERENCE_FRAGMENT_ADVANCED_CONFIG = "FragmentAdvancedConfig";
	public final static String ACTION_PREFERENCE_FRAGMENT_DEVICE_INFO_CONFIG = "FragmentDeviceInfoConfig";
	public final static String ACTION_FRAGMENT_ABOUT = "FragmentAbout";
	public final static String ACTION_FRAGMENT_HELP = "FragmentHelp";
	public final static String ACTION_PREFERENCE_FRAGMENT_SENSOR_CONFIG_CATEGORY = "FragmentSensorConfigCategory";
	public final static String ACTION_PREFERENCE_FRAGMENT_CONNECTION_CONFIG = "FragmentConnectionConfig";

	public final static String EXTRA_CONFIG_KEY = "cnf_key";

	private ActionBar mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActionBar = getActionBar();
		if (mActionBar != null) {
			mActionBar.setTitle(R.string.settings);
		}



		startService(new Intent(this, ServiceDataTransfer.class));

		Intent incomeIntent = getIntent();
		String action = incomeIntent.getAction();

		Fragment fragment;
		Bundle fragmentArgs = new Bundle();
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

		if (ACTION_PREFERENCE_FRAGMENT_APP_CONFIG.equals(action)) {
			fragment = new FragmentAppConfig();
			if(incomeIntent.hasExtra(EXTRA_CONFIG_KEY)) {
				fragmentArgs.putString(EXTRA_CONFIG_KEY, incomeIntent.getStringExtra(EXTRA_CONFIG_KEY));
			}

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_app);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_SENSOR_CONFIG_CATEGORY.equals(action)) {
			fragment = new FragmentSensorConfigCategory();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_category_sensor);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_CONNECTION_CONFIG.equals(action)) {
			fragment = new FragmentConnectionConfig();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_category_connection);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_SENSOR_CONFIG.equals(action)) {
			fragment = new FragmentSensorConfig();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_sensor);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_ADVANCED_CONFIG.equals(action)) {
			fragment = new FragmentAdvancedConfig();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_advanced);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_DEVICE_INFO_CONFIG.equals(action)) {
			fragment = new FragmentDeviceInfoConfig();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_device_info);
			}
		}

		else if (ACTION_FRAGMENT_ABOUT.equals(action)) {
			fragment = new FragmentAbout();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_about);
			}
		}

		else if (ACTION_FRAGMENT_HELP.equals(action)) {
			fragment = new FragmentHelp();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_help);
			}
		}

		else if (ACTION_PREFERENCE_FRAGMENT_LAN_CONFIG.equals(action)) {
			fragment = new FragmentLanConfig();

			if (mActionBar != null) {
				mActionBar.setTitle(R.string.pref_settings_lan);
			}
		}

		else {
			fragment = new FragmentConfigHeads();
		}

		fragment.setArguments(fragmentArgs);

		fragmentTransaction
			.add(android.R.id.content, fragment)
			.commit();

	}

	/*@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.widget_preferences_head, target);
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				getFragmentManager().popBackStack();
				return true;
		}

		return super.onOptionsItemSelected(item);
	}
}