package com.persilab.bsg.activities;

import android.app.Activity;
import android.os.Bundle;
import com.persilab.bsg.R;

/**
 * Created by konstantin on 22.09.14.
 */
public class ActivityHelp extends BaseActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
	}
}