package com.persilab.bsg.activities;

import android.app.Activity;
import android.content.*;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.persilab.bsg.R;
import com.persilab.bsg.fragments.FragmentPauseDialog;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;
import com.persilab.bsg.lib.state.State;
import com.persilab.bsg.services.ServiceDataTransfer;

/**
 * Created by Константин on 26.10.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class ActivityWidget extends BaseActivity {

	SharedPreferences preferences;
	State currentState;
	BroadcastReceiver onDeviceDataReceiver;
	FragmentPauseDialog pauseDialog;

	public static final String ACTION_PAUSE_DIALOG = "action.pause.dialog";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_widget);

		onDeviceDataReceiver = _initDeviceStateReceiver();
		preferences = PrefWrap.getPreferences(this);

		Intent dvcIntent = new Intent(this, ServiceDataTransfer.class);
		dvcIntent.setAction(ServiceDataTransfer.ACTION_DEVICE_CURRENT_STATE);
		startService(dvcIntent);

		findViewById(R.id.status).setVisibility(View.INVISIBLE);
		findViewById(R.id.temperature).setVisibility(View.INVISIBLE);
		findViewById(R.id.humidity).setVisibility(View.INVISIBLE);
		findViewById(R.id.bat).setVisibility(View.INVISIBLE);
		findViewById(R.id.lamp_state).setVisibility(View.INVISIBLE);

		findViewById(R.id.config).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ActivityWidget.this, ActivityConfig.class));
			}
		});

		findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ActivityWidget.this, ActivityConfig.class);
				intent.setAction(ActivityConfig.ACTION_FRAGMENT_ABOUT);
				startActivity(intent);
			}
		});

		findViewById(R.id.turn_off).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pauseDialog.show(getFragmentManager(), "PauseDialog");

				/*Intent pauseIntent = new Intent(ActivityWidget.this, ServiceDataTransfer.class);
				pauseIntent.setAction(ServiceDataTransfer.ACTION_WIDGET_PAUSE);
				startService(pauseIntent);*/
			}
		});

		findViewById(R.id.lamp).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ActivityWidget.this, ActivityConfig.class);
				intent.setAction(ActivityConfig.ACTION_PREFERENCE_FRAGMENT_APP_CONFIG);
				intent.putExtra(ActivityConfig.EXTRA_CONFIG_KEY, PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR);
				startActivity(intent);
			}
		});

		findViewById(R.id.nightlight_switch).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentState.getLampState() == SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF) {
					currentState.setLampState(PrefWrap.getStr(ActivityWidget.this,
							PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR));
					//((ImageButton) v).setImageResource(R.drawable.w_ic_nightlight_switch_on);
				} else {
					currentState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
					//((ImageButton) v).setImageResource(R.drawable.w_ic_nightlight_switch_off);
				}

				Intent intent = new Intent(ActivityWidget.this, ServiceDataTransfer.class);
				intent.setAction(ServiceDataTransfer.ACTION_COMMAND);
				intent.putExtra(ServiceDataTransfer.EXTRA_CMD, SocketDataPacket.CMD_SET_NIGHTLIGHT);

				SocketDataPacket.SuCmd suCmd = new SocketDataPacket.SuCmd();
				suCmd.nNightlightColor = currentState.getLampState();
				suCmd.nNightlightDurationSec = (short)PrefWrap.getInt(ActivityWidget.this,
					PrefWrap.CONFIG_KEY_SENSOR_NIGHTLIGHT_OFF);

				intent.putExtra(ServiceDataTransfer.EXTRA_DATA, suCmd.getBytes());

				startService(intent);
			}
		});

		pauseDialog = FragmentPauseDialog.newInstance(
			new FragmentPauseDialog.OnPauseDialogActionListener() {

				@Override
				public void onAccept() {
					Intent pauseIntent = new Intent(ActivityWidget.this, ServiceDataTransfer.class);
					pauseIntent.setAction(ServiceDataTransfer.ACTION_WIDGET_PAUSE);
					startService(pauseIntent);

					startActivity(new Intent(ActivityWidget.this,
						ActivityWidgetPaused.class));
					finish();
				}

				@Override
				public void onCancel() {

				}
			});
	}

	@Override
	protected void onStart() {
		super.onStart();

		Intent incomeIntent = getIntent();
		if(ACTION_PAUSE_DIALOG.equals(incomeIntent.getAction())) {
			pauseDialog.show(getFragmentManager(), "PauseDialog");
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(onDeviceDataReceiver,
				new IntentFilter(ServiceDataTransfer.ACTION_DEVICE_DATA));
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(onDeviceDataReceiver);
	}

	private BroadcastReceiver _initDeviceStateReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();

				if (ServiceDataTransfer.ACTION_DEVICE_DATA.equals(action)) {
					currentState = intent.getParcelableExtra(ServiceDataTransfer.EXTRA_STATE);

					if(
						intent.hasExtra(ServiceDataTransfer.EXTRA_PAUSED)
						&& intent.getBooleanExtra(ServiceDataTransfer.EXTRA_PAUSED, false)
					) {
						Log.d("BSG", "ActivityWidgetPaused starts");
						startActivity(new Intent(ActivityWidget.this, ActivityWidgetPaused.class));
						finish(); //finish current activity ActivityWidget
						return;
					}

					if(
						intent.hasExtra(ServiceDataTransfer.EXTRA_IS_DEMO)
						&& intent.getBooleanExtra(ServiceDataTransfer.EXTRA_IS_DEMO, false)
					) {
						findViewById(R.id.demoIndicator).setVisibility(View.VISIBLE);
					} else {
						findViewById(R.id.demoIndicator).setVisibility(View.GONE);
					}

					Resources res = context.getResources();

					findViewById(R.id.status).setVisibility(View.VISIBLE);
					findViewById(R.id.temperature).setVisibility(View.VISIBLE);
					findViewById(R.id.humidity).setVisibility(View.VISIBLE);
					findViewById(R.id.bat).setVisibility(View.VISIBLE);
					findViewById(R.id.lamp_state).setVisibility(View.VISIBLE);

					((ImageView)findViewById(R.id.face)).setImageResource(currentState.getIcon());

					((TextView)findViewById(R.id.status)).setText(currentState.getDescription(res));

					String temperature = String.format(res.getString(R.string.w_temperature_pattern), currentState.getTemperature());
					((TextView)findViewById(R.id.temperature)).setText(temperature);

					String humidity = String.format(res.getString(R.string.w_humidity_pattern), currentState.getHumidity());
					((TextView)findViewById(R.id.humidity)).setText(humidity);

					String bat = String.format(res.getString(R.string.w_bat_pattern), currentState.getBat());
					((TextView)findViewById(R.id.bat)).setText(bat);

					switch (currentState.getBatState()) {
						case State.BATTERY_STATE_CHARGE:
							findViewById(R.id.batState).setVisibility(View.GONE);
							findViewById(R.id.batStateCharging).setVisibility(View.VISIBLE);
							break;

						case State.BATTERY_STATE_EMPTY:
							findViewById(R.id.batState).setVisibility(View.VISIBLE);
							((ImageView)findViewById(R.id.batState)).setImageResource(R.drawable.w_ic_bat_empty);
							findViewById(R.id.batStateCharging).setVisibility(View.GONE);
							break;

						case State.BATTERY_STATE_LOW:
							findViewById(R.id.batState).setVisibility(View.VISIBLE);
							((ImageView)findViewById(R.id.batState)).setImageResource(R.drawable.w_ic_bat_low);
							findViewById(R.id.batStateCharging).setVisibility(View.GONE);
							break;

						case State.BATTERY_STATE_FULL:
							findViewById(R.id.batState).setVisibility(View.VISIBLE);
							((ImageView)findViewById(R.id.batState)).setImageResource(R.drawable.w_ic_bat_full);
							findViewById(R.id.batStateCharging).setVisibility(View.GONE);
							break;
					}

					((ImageView)findViewById(R.id.lamp)).setImageResource(currentState.getLampIcon());
					((TextView)findViewById(R.id.lamp_state)).setText(currentState.getLampDescription(res));

					/*if(currentState.getLampState() == SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF) {
						((ImageButton) findViewById(R.id.nightlight_switch))
							.setImageResource(R.drawable.w_ic_nightlight_switch_off);
					} else {
						((ImageButton) findViewById(R.id.nightlight_switch))
							.setImageResource(R.drawable.w_ic_nightlight_switch_on);
					}*/
				}
			}
		};
	}


}