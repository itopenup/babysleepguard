package com.persilab.bsg;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.*;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import com.persilab.bsg.activities.ActivityConfig;
import com.persilab.bsg.activities.ActivityWidget;
import com.persilab.bsg.activities.ActivityWidgetPaused;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;
import com.persilab.bsg.lib.state.*;
import com.persilab.bsg.services.ServiceDataTransfer;

/**
 * Created by konstantin on 16.09.14.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class WidgetMain extends AppWidgetProvider {

	private State currentState;

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		Log.d("BSG", "Widget received action: " + action);

		if (ServiceDataTransfer.ACTION_DEVICE_DATA.equals(action)) {
			currentState = intent.getParcelableExtra(ServiceDataTransfer.EXTRA_STATE);

			ComponentName thisAppWidget = new ComponentName(context, WidgetMain.class);
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			int appWidgetIds[] = appWidgetManager.getAppWidgetIds(thisAppWidget);

			if (appWidgetIds != null && appWidgetIds.length > 0) {
				if(
					intent.hasExtra(ServiceDataTransfer.EXTRA_PAUSED)
					&& intent.getBooleanExtra(ServiceDataTransfer.EXTRA_PAUSED, false)
				) {
					onPausedUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
				} else {
					onUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
				}
			}
		}

		if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {
			//Retrieve device state immediately, for initialize just added widget
			Intent dvcIntent = new Intent(context, ServiceDataTransfer.class);
			dvcIntent.setAction(ServiceDataTransfer.ACTION_DEVICE_CURRENT_STATE);
			context.startService(dvcIntent);

			Bundle extras = intent.getExtras();
			if (extras != null) {
				int[] appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);
				if (appWidgetIds != null && appWidgetIds.length > 0) {
					onPausedUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
				}
			}
		}
		else if (AppWidgetManager.ACTION_APPWIDGET_DELETED.equals(action)) {
			Bundle extras = intent.getExtras();
			if (extras != null && extras.containsKey(AppWidgetManager.EXTRA_APPWIDGET_ID)) {
				final int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
				this.onDeleted(context, new int[] { appWidgetId });
			}
		}
		else if (AppWidgetManager.ACTION_APPWIDGET_ENABLED.equals(action)) {
			this.onEnabled(context);
		}
		else if (AppWidgetManager.ACTION_APPWIDGET_DISABLED.equals(action)) {
			this.onDisabled(context);
		}
	}

	@Override
	public void onEnabled(Context context) {
		Intent dvcIntent = new Intent(context, ServiceDataTransfer.class);
		dvcIntent.setAction(ServiceDataTransfer.ACTION_DEVICE_CURRENT_STATE);
		context.startService(dvcIntent);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		//Log.d("BSG", "Widget deleted - deleted one of instances");
	}

	@Override
	public void onDisabled(Context context) {
		//Log.d("BSG", "Widget disabled - deleted last of instances");
	}

	public void onPausedUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		for (int widgetID : appWidgetIds) {
			RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget_paused);
			PendingIntent pIntent;

			//Info button
			Intent infoIntent = new Intent(context, ActivityConfig.class);
			infoIntent.setAction(ActivityConfig.ACTION_FRAGMENT_ABOUT);
			pIntent = PendingIntent.getActivity(context, widgetID, infoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.info, pIntent);

			Intent resumeIntent = new Intent(context, ActivityWidgetPaused.class);
			resumeIntent.setAction(ActivityWidgetPaused.ACTION_RESUME_DIALOG);
			pIntent = PendingIntent.getActivity(context, 0, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.turn_on, pIntent);

			appWidgetManager.updateAppWidget(widgetID, widgetView);
		}
	}

	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		for (int widgetID : appWidgetIds) {
			RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget);
			PendingIntent pIntent;
			Resources res = context.getResources();

			if(PrefWrap.getBool(context, PrefWrap.CONFIG_KEY_APP_DEMO_MODE)) {
				widgetView.setViewVisibility(R.id.demoIndicator, View.VISIBLE);
			} else {
				widgetView.setViewVisibility(R.id.demoIndicator, View.GONE);
			}

			if(currentState == null) {
				currentState = new StateNoConnection();
				currentState.setTemperature(0);
				currentState.setHumidity(0);
				currentState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
				currentState.setBat(0);

				widgetView.setViewVisibility(R.id.status, View.INVISIBLE);
				widgetView.setViewVisibility(R.id.temperature, View.INVISIBLE);
				widgetView.setViewVisibility(R.id.humidity, View.INVISIBLE);
				widgetView.setViewVisibility(R.id.bat, View.INVISIBLE);
				widgetView.setViewVisibility(R.id.lamp_state, View.INVISIBLE);
			} else {
				widgetView.setViewVisibility(R.id.status, View.VISIBLE);
				widgetView.setViewVisibility(R.id.temperature, View.VISIBLE);
				widgetView.setViewVisibility(R.id.humidity, View.VISIBLE);
				widgetView.setViewVisibility(R.id.bat, View.VISIBLE);
				widgetView.setViewVisibility(R.id.lamp_state, View.VISIBLE);

				widgetView.setImageViewResource(R.id.face, currentState.getIcon());

				widgetView.setTextViewText(R.id.status, currentState.getDescription(res));

				String temperature = String.format(res.getString(R.string.w_temperature_pattern), currentState.getTemperature());
				widgetView.setTextViewText(R.id.temperature, temperature);

				String humidity = String.format(res.getString(R.string.w_humidity_pattern), currentState.getHumidity());
				widgetView.setTextViewText(R.id.humidity, humidity);

				String bat = String.format(res.getString(R.string.w_bat_pattern), currentState.getBat());
				widgetView.setTextViewText(R.id.bat, bat);

				switch (currentState.getBatState()) {
					case State.BATTERY_STATE_CHARGE:
						widgetView.setViewVisibility(R.id.batState, View.GONE);
						widgetView.setViewVisibility(R.id.batStateCharging, View.VISIBLE);
						break;

					case State.BATTERY_STATE_EMPTY:
						widgetView.setViewVisibility(R.id.batState, View.VISIBLE);
						widgetView.setViewVisibility(R.id.batStateCharging, View.GONE);
						widgetView.setImageViewResource(R.id.batState, R.drawable.w_ic_bat_empty);
						break;

					case State.BATTERY_STATE_LOW:
						widgetView.setViewVisibility(R.id.batState, View.VISIBLE);
						widgetView.setViewVisibility(R.id.batStateCharging, View.GONE);
						widgetView.setImageViewResource(R.id.batState, R.drawable.w_ic_bat_low);
						break;

					case State.BATTERY_STATE_FULL:
						widgetView.setViewVisibility(R.id.batState, View.VISIBLE);
						widgetView.setViewVisibility(R.id.batStateCharging, View.GONE);
						widgetView.setImageViewResource(R.id.batState, R.drawable.w_ic_bat_full);
						break;
				}


				widgetView.setImageViewResource(R.id.lamp, currentState.getLampIcon());
				widgetView.setTextViewText(R.id.lamp_state, currentState.getLampDescription(res));

				/*if(currentState.getLampState() == SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF) {
					widgetView.setImageViewResource(R.id.nightlight_switch, R.drawable.w_ic_nightlight_switch_off);
				} else {
					widgetView.setImageViewResource(R.id.nightlight_switch, R.drawable.w_ic_nightlight_switch_on);
				}*/
			}

			//Settings button
			Intent configIntent = new Intent(context, ActivityConfig.class);
			configIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
			configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
			pIntent = PendingIntent.getActivity(context, widgetID, configIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.config, pIntent);

			//Face
			Intent mainIntent = new Intent(context, ActivityWidget.class);
			//mainIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
			//mainIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
			pIntent = PendingIntent.getActivity(context, widgetID, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.face, pIntent);

			//Info button
			Intent infoIntent = new Intent(context, ActivityConfig.class);
			infoIntent.setAction(ActivityConfig.ACTION_FRAGMENT_ABOUT);
			pIntent = PendingIntent.getActivity(context, widgetID, infoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.info, pIntent);

			//Lamp switch button
			Intent lampColorIntent = new Intent(context, ActivityConfig.class);
			lampColorIntent.setAction(ActivityConfig.ACTION_PREFERENCE_FRAGMENT_APP_CONFIG);
			lampColorIntent.putExtra(ActivityConfig.EXTRA_CONFIG_KEY, PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR);
			pIntent = PendingIntent.getActivity(context, widgetID, lampColorIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.lamp, pIntent);

			//Lamp turn off/on button
			Intent nightLightIntent = new Intent(context, ServiceDataTransfer.class);
			nightLightIntent.setAction(ServiceDataTransfer.ACTION_COMMAND);
			nightLightIntent.putExtra(ServiceDataTransfer.EXTRA_CMD, SocketDataPacket.CMD_SET_NIGHTLIGHT);
			SocketDataPacket.SuCmd suCmd = new SocketDataPacket.SuCmd();
			if(currentState.getLampState() == SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF) {
				currentState.setLampState(PrefWrap.getStr(context,
					PrefWrap.CONFIG_KEY_APP_NIGHTLIGHT_COLOR));
			} else {
				currentState.setLampState(SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF);
			}
			suCmd.nNightlightColor = currentState.getLampState();
			suCmd.nNightlightDurationSec = (short)PrefWrap.getInt(context,
				PrefWrap.CONFIG_KEY_SENSOR_NIGHTLIGHT_OFF);
			nightLightIntent.putExtra(ServiceDataTransfer.EXTRA_DATA, suCmd.getBytes());
			pIntent = PendingIntent.getService(context, widgetID, nightLightIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.nightlight_switch, pIntent);

			//Turn-off
			Intent pauseIntent = new Intent(context, ActivityWidget.class);
			pauseIntent.setAction(ActivityWidget.ACTION_PAUSE_DIALOG);
			pIntent = PendingIntent.getActivity(context, widgetID, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			widgetView.setOnClickPendingIntent(R.id.turn_off, pIntent);

			appWidgetManager.updateAppWidget(widgetID, widgetView);
		}
	}
}
