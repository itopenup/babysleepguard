package com.persilab.bsg.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TaskLogFile extends AsyncTask<String, Void, Void> {

	private File logFile;

	public TaskLogFile(Context context, String fileName) {
		super();

		File logsDir = getLogsDir(context);
		if(logsDir != null && (logsDir.mkdirs() || logsDir.exists())) {
			logFile = new File(logsDir, fileName);
		}
	}

	public static File getLogsDir(Context context) {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return new File(context.getExternalFilesDir(null), "logs");
		}

		return null;
	}

	@Override
	protected Void doInBackground(String... params) {
		if(logFile == null) {
			return null;
		}

		FileOutputStream outputStream;

		try {
			outputStream = new FileOutputStream(logFile, true);

			for(String str : params) {
				outputStream.write(str.getBytes());
			}

			outputStream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
