package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateVoice extends State {

	public StateVoice() {
		super();

		icon = R.drawable.w_ic_face_voice;
		description = R.string.w_status_voice;
		properties.put(PROP_ALERT, new Property(true, Property.OPERATOR_AND)); //1 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		return res.getString(description);
	}


	/**
	 * Parcelable methods
	 */
	public static final Parcelable.Creator<StateVoice> CREATOR = new Parcelable.Creator<StateVoice>() {
		public StateVoice createFromParcel(Parcel in) {
			return new StateVoice(in);
		}

		public StateVoice[] newArray(int size) {
			return new StateVoice[size];
		}
	};

	private StateVoice (Parcel parcel) {
		super(parcel);
	}
}
