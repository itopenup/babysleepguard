package com.persilab.bsg.lib.state;

import java.io.Serializable;

/**
 * Logical operation reference:
 *
 * * AND:
 * 1 -> 1 = 1
 * 1 -> 0 = 0
 * 0 -> 1 = 0
 * 0 -> 0 = 0
 *
 * * OR:
 * 1 -> 1 = 1
 * 1 -> 0 = 1
 * 0 -> 1 = 1
 * 0 -> 0 = 0
 *
 * * IMPL (implication, effect, result):
 * 1 -> 1 = 1
 * 1 -> 0 = 0
 * 0 -> 1 = 1
 * 0 -> 0 = 1
 */

public class Property implements Serializable {
	public static final byte OPERATOR_AND = 1;
	public static final byte OPERATOR_OR = 2;
	public static final byte OPERATOR_IMPL = 3;

	private boolean value;
	private byte operator;

	/**
	 * Description
	 * @param v thanks
	 * @param op byte
	 */
	public Property(boolean v, byte op) {
		value = v;
		operator = op;
	}

	public boolean jump(boolean v) {
		switch(operator) {
			case OPERATOR_AND:
				value = value && v;
				break;

			case OPERATOR_OR:
				value = value || v;
				break;

			case OPERATOR_IMPL:
				value = (!value || v);
				break;
		}

		return value;
	}

	public boolean getValue() {
		return this.value;
	}

	public byte getOperator() {
		return operator;
	}
}
