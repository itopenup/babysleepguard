package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateAwake extends State {

	private long time;

	public StateAwake() {
		super();

		icon = R.drawable.w_ic_face_normal;
		description = R.string.w_status_awake;
		properties.put(PROP_ALERT, new Property(false, Property.OPERATOR_OR)); //0 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		long h = (long)Math.floor(time/3600);
		long m = (long)Math.floor((time - h*3600)/60);
		long s = time - h*3600 - m*60;

		return String.format(res.getString(description), h, m, s);
	}

	public void setTime(long t) {
		time = t;
	}


	/**
	 * Parcelable methods
	 */
	public void writeToParcel(Parcel parcel, int flags) {
		super.writeToParcel(parcel, flags);
		parcel.writeLong(time);
	}

	public static final Parcelable.Creator<StateAwake> CREATOR = new Parcelable.Creator<StateAwake>() {
		public StateAwake createFromParcel(Parcel in) {
			return new StateAwake(in);
		}

		public StateAwake[] newArray(int size) {
			return new StateAwake[size];
		}
	};

	private StateAwake (Parcel parcel) {
		super(parcel);
		time = parcel.readInt();
	}
}
