package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateConnecting extends State {

	public StateConnecting() {
		super();

		icon = R.drawable.w_ic_face_not_connected;
		description = R.string.w_status_connecting;
		properties.put(PROP_ALERT, new Property(false, Property.OPERATOR_OR)); //0 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		return res.getString(description);
	}



	/**
	 * Parcelable methods
	 */
	public static final Creator<StateConnecting> CREATOR = new Creator<StateConnecting>() {
		public StateConnecting createFromParcel(Parcel in) {
			return new StateConnecting(in);
		}

		public StateConnecting[] newArray(int size) {
			return new StateConnecting[size];
		}
	};

	private StateConnecting(Parcel parcel) {
		super(parcel);
	}
}
