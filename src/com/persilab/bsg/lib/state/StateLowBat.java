package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateLowBat extends State {

	public StateLowBat() {
		super();

		icon = R.drawable.w_ic_face_low_bat;
		description = R.string.w_status_low_bat;
		properties.put(PROP_ALERT, new Property(true, Property.OPERATOR_AND)); //1 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		return res.getString(description);
	}



	/**
	 * Parcelable methods
	 */
	public static final Parcelable.Creator<StateLowBat> CREATOR = new Parcelable.Creator<StateLowBat>() {
		public StateLowBat createFromParcel(Parcel in) {
			return new StateLowBat(in);
		}

		public StateLowBat[] newArray(int size) {
			return new StateLowBat[size];
		}
	};

	private StateLowBat (Parcel parcel) {
		super(parcel);
	}
}
