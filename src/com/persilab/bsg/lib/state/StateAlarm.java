package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateAlarm extends State {

	private long time;

	public StateAlarm() {
		super();

		icon = R.drawable.w_ic_face_nobody;
		properties.put(PROP_ALERT, new Property(true, Property.OPERATOR_AND)); //1 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		if(time > 0) {
			long h = (long)Math.floor(time/3600);
			long m = (long)Math.floor((time - h*3600)/60);
			long s = time - h*3600 - m*60;

			return String.format(res.getString(description), h, m, s);
		}

		return String.format(res.getString(description), 0, 0, 0);
	}

	public void setNobody(long t) {
		time = t;
		icon = R.drawable.w_ic_face_nobody;
		description = R.string.w_status_alarm_absence;
	}

	public void setFrequentBreath() {
		time = 0;
		icon = R.drawable.w_ic_face_frequent_breath;
		description = R.string.w_status_alarm_frequent_breath;
	}

	public void setFrequentPulse() {
		time = 0;
		icon = R.drawable.w_ic_face_frequent_pulse;
		description = R.string.w_status_alarm_frequent_pulse;
	}

	public void setRareBreath() {
		time = 0;
		icon = R.drawable.w_ic_face_rare_breath;
		description = R.string.w_status_alarm_rare_breath;
	}

	public void setRarePulse() {
		time = 0;
		icon = R.drawable.w_ic_face_rare_pulse;
		description = R.string.w_status_alarm_rare_pulse;
	}


	/**
	 * Parcelable methods
	 */
	public void writeToParcel(Parcel parcel, int flags) {
		super.writeToParcel(parcel, flags);
		parcel.writeLong(time);
	}

	public static final Parcelable.Creator<StateAlarm> CREATOR = new Parcelable.Creator<StateAlarm>() {
		public StateAlarm createFromParcel(Parcel in) {
			return new StateAlarm(in);
		}
		public StateAlarm[] newArray(int size) {
			return new StateAlarm[size];
		}
	};

	private StateAlarm (Parcel parcel) {
		super(parcel);
		time = parcel.readInt();
	}
}
