package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateNoConnection extends State {

	public StateNoConnection() {
		super();

		icon = R.drawable.w_ic_face_not_connected;
		description = R.string.w_status_not_connected;
		properties.put(PROP_ALERT, new Property(false, Property.OPERATOR_OR)); //0 -> X = X
	}

	@Override
	public String getDescription(Resources res) {
		return res.getString(description);
	}



	/**
	 * Parcelable methods
	 */
	public static final Parcelable.Creator<StateNoConnection> CREATOR = new Parcelable.Creator<StateNoConnection>() {
		public StateNoConnection createFromParcel(Parcel in) {
			return new StateNoConnection(in);
		}

		public StateNoConnection[] newArray(int size) {
			return new StateNoConnection[size];
		}
	};

	private StateNoConnection (Parcel parcel) {
		super(parcel);
	}
}
