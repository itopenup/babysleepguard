package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;

/**
 * Created by Константин on 18.10.2014.
 */
public class StateSleep extends State {

	long time;
	int breath, pulse;
	boolean motion;

	public StateSleep() {
		super();

		icon = R.drawable.w_ic_face_sleep;
		description = R.string.w_status_sleep;
		properties.put(PROP_ALERT, new Property(false, Property.OPERATOR_IMPL)); //0 -> X = 1
	}

	public void setTime(long t) {
		time = t;
	}

	public void setBreath(int b) {
		breath = b;
	}

	public void setPulse(int p) {
		pulse = p;
	}

	public void setMotion(boolean m) {
		motion = m;
	}

	@Override
	public String getDescription(Resources res) {
		long h = (long)Math.floor(time/3600);
		long m = (long)Math.floor((time - h*3600)/60);
		long s = time - h*3600 - m*60;

		return String.format(res.getString(description), h, m, s, breath, pulse,
			(motion ? res.getString(R.string.w_motion_yes) : res.getString(R.string.w_motion_no)));
	}


	/**
	 * Parcelable methods
	 */
	public void writeToParcel(Parcel parcel, int flags) {
		super.writeToParcel(parcel, flags);

		parcel.writeLong(time);
		parcel.writeInt(breath);
		parcel.writeInt(pulse);
		parcel.writeBooleanArray(new boolean[] {motion});
	}

	public static final Parcelable.Creator<StateSleep> CREATOR = new Parcelable.Creator<StateSleep>() {
		public StateSleep createFromParcel(Parcel in) {
			return new StateSleep(in);
		}

		public StateSleep[] newArray(int size) {
			return new StateSleep[size];
		}
	};

	private StateSleep (Parcel parcel) {
		super(parcel);
	}
}
