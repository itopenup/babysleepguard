package com.persilab.bsg.lib.state;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import com.persilab.bsg.R;
import com.persilab.bsg.lib.PrefWrap;
import com.persilab.bsg.lib.SocketDataProcessor.SocketDataPacket;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class State implements Parcelable {

	public static final String PROP_ALERT = "alert";

	public static final int BATTERY_STATE_CHARGE = -1;
	public static final int BATTERY_STATE_EMPTY = 0;
	public static final int BATTERY_STATE_LOW = 1;
	public static final int BATTERY_STATE_FULL = 2;

	/**
	 * Icon drawable resource
	 */
	protected int icon;

	/**
	 * Description string resource
	 */
	protected int description;



	/**
	 * Other state values
	 */
	protected int temperature;
	protected int humidity;
	protected int bat;
	protected int batState;
	protected SocketDataPacket.NightlightColor lamp;

	/**
	 * Properties to be changed by their operators
	 */
	protected HashMap<String, Property> properties;

	public State() {
		properties = new HashMap<String, Property>();
	}

	public Class<?> type() {
		return super.getClass();
	}

	public HashMap<String, Property> getProperties() {
		return properties;
	}

	public HashMap<String, Property> jump(State newState) {
		HashMap<String, Property> newProperties = newState.getProperties();

		for(HashMap.Entry<String, Property> pair : properties.entrySet()) {
			String key = pair.getKey();

			if(newProperties.containsKey(key)) {
				pair.getValue().jump(newProperties.get(key).getValue());
			}
		}

		return properties;
	}

	public int getIcon() {
		return this.icon;
	}


	public int getTemperature() {
		return this.temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getHumidity() {
		return this.humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getBat() {
		return this.bat;
	}

	public void setBat(int bat) {
		this.bat = bat;
	}

	public int getBatState() {
		return this.batState;
	}

	public void setBatState(int state) {
		this.batState = state;
	}

	public void setLampState(SocketDataPacket.NightlightColor lstate) {
		this.lamp = lstate;
	}

	public void setLampState(String lstate) {
		if (PrefWrap.NIGHTLIGHT_COLOR_OFF.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_BLUE.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_BLUE;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_CYAN.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_CYAN;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_GREEN.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_GREEN;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_MAGENTA.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_MAGENTA;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_RED.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_RED;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_WHITE.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_WHITE;
		}
		else if (PrefWrap.NIGHTLIGHT_COLOR_YELLOW.equals(lstate)) {
			this.lamp = SocketDataPacket.NightlightColor.NIGHTLIGHT_YELLOW;
		}
	}

	public SocketDataPacket.NightlightColor getLampState() {
		return this.lamp;
	}

	public int getLampIcon() {
		switch (this.lamp) {
			case NIGHTLIGHT_OFF:
				return R.drawable.w_ic_lamp_off;

			case NIGHTLIGHT_BLUE:
				return R.drawable.w_ic_lamp_blue;

			case NIGHTLIGHT_CYAN:
				return R.drawable.w_ic_lamp_cyan;

			case NIGHTLIGHT_GREEN:
				return R.drawable.w_ic_lamp_green;

			case NIGHTLIGHT_MAGENTA:
				return R.drawable.w_ic_lamp_violet;

			case NIGHTLIGHT_RED:
				return R.drawable.w_ic_lamp_red;

			case NIGHTLIGHT_WHITE:
				return R.drawable.w_ic_lamp_white;

			case NIGHTLIGHT_YELLOW:
				return R.drawable.w_ic_lamp_yellow;
		}

		return R.drawable.w_ic_lamp_off;
	}

	public String getLampDescription(Resources res) {
		return this.lamp == SocketDataPacket.NightlightColor.NIGHTLIGHT_OFF
			? res.getString(R.string.w_lamp_off)
			: res.getString(R.string.w_lamp_on);
	}

	abstract public String getDescription(Resources res);



	/**
	 * Parcelable method
	 */
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(this.icon);
		parcel.writeInt(this.description);
		parcel.writeInt(this.temperature);
		parcel.writeInt(this.humidity);
		parcel.writeInt(this.bat);
		parcel.writeInt(this.batState);
		parcel.writeSerializable(this.lamp);
		parcel.writeSerializable(this.properties);
	}

	protected State(Parcel parcel) {
		this.icon = parcel.readInt();
		this.description = parcel.readInt();
		this.temperature = parcel.readInt();
		this.humidity = parcel.readInt();
		this.bat = parcel.readInt();
		this.batState = parcel.readInt();
		this.lamp = (SocketDataPacket.NightlightColor) parcel.readSerializable();

		this.properties = new HashMap<String, Property>();
		this.properties = (HashMap<String, Property>) parcel.readSerializable();
	}
}
