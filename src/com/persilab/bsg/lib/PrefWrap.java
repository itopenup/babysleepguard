package com.persilab.bsg.lib;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Константин on 30.10.2014.
 * All right reserved.
 * For information about this code please email me
 * it.openup+android@gmail.com
 */

public class PrefWrap {

	public static final String ALARM_TYPE_OFF = "OFF";
	public static final String ALARM_TYPE_SOUND = "SOUND";
	public static final String ALARM_TYPE_VIBRATE = "VIBRATE";
	public static final String ALARM_TYPE_SOUND_VIBRATE = "SOUND_VIBRATE";

	public static final String ALARM_TYPE_TONE_DEFAULT = "content://settings/system/notification_sound";

	public static final String NIGHTLIGHT_COLOR_OFF = "NIGHTLIGHT_OFF";
	public static final String NIGHTLIGHT_COLOR_RANDOM = "NIGHTLIGHT_RANDOM";
	public static final String NIGHTLIGHT_COLOR_RED = "NIGHTLIGHT_RED";
	public static final String NIGHTLIGHT_COLOR_GREEN = "NIGHTLIGHT_GREEN";
	public static final String NIGHTLIGHT_COLOR_BLUE = "NIGHTLIGHT_BLUE";
	public static final String NIGHTLIGHT_COLOR_MAGENTA = "NIGHTLIGHT_MAGENTA";
	public static final String NIGHTLIGHT_COLOR_CYAN = "NIGHTLIGHT_CYAN";
	public static final String NIGHTLIGHT_COLOR_YELLOW = "NIGHTLIGHT_YELLOW";
	public static final String NIGHTLIGHT_COLOR_WHITE = "NIGHTLIGHT_WHITE";

	public static final String CONFIG_KEY_APP_LANG = "pref_settings_app_lang";
	public static final String CONFIG_KEY_APP_SHOW_LOG = "pref_settings_app_show_log";
	public static final String CONFIG_KEY_APP_REQUEST_INTERVAL = "pref_settings_app_sensor_requests_interval";
	public static final String CONFIG_KEY_APP_ALARM_PAUSE_TIME = "pref_settings_app_alarm_pause_time";
	public static final String CONFIG_KEY_APP_NIGHTLIGHT_COLOR = "pref_settings_app_night_light_color";
	public static final String CONFIG_KEY_APP_SCREEN_TURN_OFF_TIME = "pref_settings_app_screen_turnoff_time";
	public static final String CONFIG_KEY_APP_ALARM_TYPE = "pref_settings_app_alarm_type";
	public static final String CONFIG_KEY_APP_ALARM_TONE = "pref_settings_app_alarm_tone";
	public static final String CONFIG_KEY_APP_LOG_ENABLED = "pref_settings_app_write_log";
	public static final String CONFIG_KEY_APP_DEMO_MODE = "pref_settings_app_demo";

	public static final String CONFIG_KEY_ADVANCED_NOISE_TRESHOLD = "pref_settings_advanced_noise_threshold";
	public static final String CONFIG_KEY_ADVANCED_MOTION_TRESHOLD = "pref_settings_advanced_movement_threshold";
	public static final String CONFIG_KEY_ADVANCED_ALARM_DELAY_ON = "pref_settings_advanced_alarm_on_delay";
	public static final String CONFIG_KEY_ADVANCED_ALARM_DELAY_OFF = "pref_settings_advanced_alarm_off_delay";
	public static final String CONFIG_KEY_ADVANCED_BREATH_LOW = "pref_settings_advanced_breath_bottom_threshold";
	public static final String CONFIG_KEY_ADVANCED_BREATH_HIGH = "pref_settings_advanced_breath_top_threshold";
	public static final String CONFIG_KEY_ADVANCED_PULSE_LOW = "pref_settings_advanced_pulse_bottom_threshold";
	public static final String CONFIG_KEY_ADVANCED_PULSE_HIGH = "pref_settings_advanced_pulse_top_threshold";
	public static final String CONFIG_KEY_ADVANCED_NOISE_REACT_TIME = "pref_settings_advanced_noise_react_time";
	public static final String CONFIG_KEY_ADVANCED_MOTION_REACT_TIME = "pref_settings_advanced_move_react_time";

	public static final String CONFIG_KEY_SENSOR_NIGHTLIGHT_OFF = "pref_settings_sensor_auto_off_nightlight";

	public static final String CONFIG_KEY_LAN_IP = "pref_settings_lan_default_ip";
	public static final String CONFIG_KEY_LAN_PORT = "pref_settings_lan_default_port";

	public static final String CONFIG_KEY_CONNECTION_IP = "pref_settings_connection_ip";
	public static final String CONFIG_KEY_CONNECTION_PORT = "pref_settings_connection_port";

	public static final String PREFERENCES_NAME = "com.persilab.bsg.preferences";
	private static SharedPreferences preferences;

	/**
	 * Is not thread-safe
	 */
	public static SharedPreferences getPreferences(Context context) {
		if (preferences == null) {
			preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		}

		return preferences;
	}

	public static int getInt(Context context, final String key) {
		try {
			return Integer.parseInt(getPreferences(context)
				.getString(key, ""));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static double getDouble(Context context, final String key) {
		try {
			return Double.parseDouble(getPreferences(context)
				.getString(key, ""));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return 0.0;
	}

	public static String getStr(Context context, final String key) {
		return getPreferences(context)
			.getString(key, "");
	}

	public static String getStr(Context context, final String key, final String defaultValue) {
		return getPreferences(context)
				.getString(key, defaultValue);
	}

	public static boolean getBool(Context context, final String key) {
		return getPreferences(context)
			.getBoolean(key, true);
	}
}
