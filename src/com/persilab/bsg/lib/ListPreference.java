package com.persilab.bsg.lib;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by konstantin on 06.11.14.
 */
public class ListPreference extends android.preference.ListPreference {
	public ListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ListPreference(Context context) {
		super(context);
	}

	public void show() {
		showDialog(null);
	}
}
