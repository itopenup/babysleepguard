package com.persilab.bsg.lib;

import android.content.Context;
import com.persilab.bsg.tasks.TaskLogFile;

import java.net.ContentHandler;

/**
 * Created by konstantin on 05.11.14.
 */
public class Logger {

	private static final String NAME_PATTERN = "bsg_pu_%tF_%tH-%tM.txt";
	private static final String LOG_STRING_PATTERN = "LOG: %s\n";

	private Context context;
	private String logFileName;
	private boolean enabled;

	public static Logger getInstance(Context context) {
		return new Logger(context);
	}

	private Logger(Context context) {
		long currentTime = System.currentTimeMillis();

		this.context = context;
		this.logFileName = String.format(NAME_PATTERN, currentTime, currentTime, currentTime);
	}

	public void log(String... args) {
		if(!enabled) {
			return;
		}

		long currentTime = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();

		sb
			.append(String.format("%tH-%tM-%tS", currentTime, currentTime, currentTime))
			.append("\t");

		for(String str : args) {
			sb.append(str).append("\t");
		}

		_write(String.format(LOG_STRING_PATTERN, sb.toString()));
	}

	public void setEnabled(boolean val) {
		this.enabled = val;
	}

	private void _write(String string) {
		TaskLogFile writeTask = new TaskLogFile(context, logFileName);
		writeTask.execute(string);
	}
}
