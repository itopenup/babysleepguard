package com.persilab.bsg.lib.SocketDataProcessor;

/**
 * Created by Константин on 04.10.2014.
 */
public interface OnSocketClientListener {
	void onSocketClientResponse(byte cmd, byte[] data);
	void onSocketClientError(byte cmd);
}
