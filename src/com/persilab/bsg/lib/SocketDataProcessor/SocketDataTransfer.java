package com.persilab.bsg.lib.SocketDataProcessor;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.EmptyStackException;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Константин on 05.10.2014.
 */
public class SocketDataTransfer {

	/**
	 * Stop thread if stack is empty for this timeout;
	 * Milliseconds: 3600000 ms = 1 hour
	 */
	private final long STACK_IS_EMPTY_TIMEOUT = 3600000;

	/**
	 * Sending commands interval
	 * Milliseconds
	 */
	private final long THREAD_SEND_CMD_INTERVAL = 500;

	private final int SOCKET_CONNECT_TIMEOUT = 10000;

	private static final int CMD_THREAD_MSG_SENT_REQUEST = 1;
	private static final int CMD_THREAD_MSG_GOT_RESPONSE = 2;
	private static final int CMD_THREAD_MSG_GOT_ERROR = 3;

	private static final int CONNECT_THREAD_MSG_CONNECTED = 1;
	private static final int CONNECT_THREAD_MSG_ERROR = 2;
	private static final int CONNECT_THREAD_MSG_NOT_CONNECTED = 3;

	private Socket mSocket;
	private String mSoServerIP;
	private int mSoServerPort;
	private Thread mCmdThread;
	private Handler mCmdHandler, mConnectHandler;
	private long mLastCmdAddedTime;
	private ConcurrentLinkedQueue<SocketDataPacket> mCmdStack;
	private OnSocketClientListener mSoListener;
	private boolean mFlagInterrupt, mConnecting;


	public SocketDataTransfer(String ip, int port, OnSocketClientListener listener) {
		mSoServerIP = ip;
		mSoServerPort = port;
		mSoListener = listener;
		mCmdStack = new ConcurrentLinkedQueue<SocketDataPacket>();
		mLastCmdAddedTime = System.currentTimeMillis();
		mFlagInterrupt = false;
		mConnecting = false;

		mCmdHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				byte cmd = (byte) msg.arg1;
				int length = msg.arg2;

				switch (msg.what) {
					case CMD_THREAD_MSG_SENT_REQUEST:
						Log.d("SOC", "Socket works: what = SENT_REQUEST; cmd = " + String.valueOf(cmd) + ";");
						break;

					case CMD_THREAD_MSG_GOT_RESPONSE:
						byte[] data = (byte[]) msg.obj;
						String response = new String(data);
						Log.d("SOC", "Socket works: what = GOT_RESPONSE; cmd = " + String.valueOf(cmd) + "; response = " + response);
						mSoListener.onSocketClientResponse(cmd, data);
						break;

					case CMD_THREAD_MSG_GOT_ERROR:
						Log.d("SOC", "Socket works: what = GOT_ERROR; cmd = " + String.valueOf(cmd) + ";");
						mSoListener.onSocketClientError(cmd);
						break;
				}
				return false;
			}
		});

		mConnectHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
					case CONNECT_THREAD_MSG_CONNECTED:
						mCmdThread = null;
						mFlagInterrupt = false;
						mCmdThread = _createCmdThread();
						mCmdThread.start();
						Log.d("SOC", "Socket is connected");
						break;

					case CONNECT_THREAD_MSG_NOT_CONNECTED:
						break;

					case CONNECT_THREAD_MSG_ERROR:
						break;
				}

				return false;
			}
		});

		restart();
	}

	/**
	 * For change
	 * @param ip
	 */
	public boolean reconnect(String ip, int port) {
		Log.d("SOC", "Reconnect socket with: ip " + ip + ", port " + String.valueOf(port));
		mSoServerIP = ip;
		mSoServerPort = port;
		return restart();
	}

	public void pushCmd(SocketDataPacket data) {
		mLastCmdAddedTime = System.currentTimeMillis();
		mCmdStack.add(data);

		if(mSocket != null) {
			Log.d("SOC", "Socket info: "
				+ "\n -> Local IP: " + String.valueOf(mSocket.getLocalAddress().getHostAddress())
				+ "\n -> Connected: " + String.valueOf(mSocket.isConnected())
				+ "\n -> Bound: " + String.valueOf(mSocket.isBound())
				+ "\n -> Closed: " + String.valueOf(mSocket.isClosed())
				+ "\n -> Connecting: " + String.valueOf(mConnecting)
			);
		} else {
			Log.d("SOC", "Socket info: "
					+ "\n -> Is NULL"
			);
		}
	}

	public boolean isConnected() {
		return mSocket != null && mSocket.isConnected();
	}

	public boolean isConnecting() {
		return mConnecting;
	}

	public void interrupt() {
		mCmdStack.clear();
		mFlagInterrupt = true;
		Log.d("SOC", "Disconnect socket");

		if(isConnected()) {
			try {
				mSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		mSocket = null;
	}

	public boolean restart() {
		interrupt();
		Log.d("SOC", "Socket info: "
				+ "\n -> Going to connect"
				+ "\n -> connecting var is " + String.valueOf(mConnecting)
		);

		mConnecting = true;

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Log.d("SOC", "Socket info: "
							+ "\n -> Connecting..."
					);


					//mSocket = new Socket(InetAddress.getByName(mSoServerIP), mSoServerPort);
					mSocket = new Socket();
					mSocket.connect(new InetSocketAddress(mSoServerIP, mSoServerPort), SOCKET_CONNECT_TIMEOUT);

					/*
					Can`t use connect method because of it is not blocking
					it just tries to connect and go out
					then the socket connects or not, who cares

					mSocket.connect(new InetSocketAddress(InetAddress.getByName(mSoServerIP), mSoServerPort),
						SOCKET_CONNECT_TIMEOUT);*/

					if(mSocket.isConnected()) {
						mConnectHandler.sendEmptyMessage(CONNECT_THREAD_MSG_CONNECTED);
						Log.d("SOC", "Socket info: "
								+ "\n -> Connected :)"
						);
					} else {
						mConnectHandler.sendEmptyMessage(CONNECT_THREAD_MSG_NOT_CONNECTED);

						Log.d("SOC", "Socket info: "
								+ "\n -> Not connected :("
						);
					}
				} catch (IOException e) {
					e.printStackTrace();
					mConnectHandler.sendEmptyMessage(CONNECT_THREAD_MSG_ERROR);

					Log.d("SOC", "Socket info: "
							+ "\n -> Connecting error :("
					);
				} finally {
					mConnecting = false;
				}
			}
		}).start();

		Log.d("SOC", "Socket info: "
				+ "\n -> Connecting started"
				+ "\n -> connecting var is " + String.valueOf(mConnecting)
		);

		return true;
	}

	private Thread _createCmdThread() {
		return new Thread(new Runnable() {
			@Override
			public void run() {
				long currentTime = System.currentTimeMillis();
				//boolean connecting = false;

				while(!mFlagInterrupt && (mCmdStack.size() > 0 || currentTime - mLastCmdAddedTime <= STACK_IS_EMPTY_TIMEOUT)) {

					if(mSocket != null && mSocket.isConnected()) {
						SocketDataPacket packet = mCmdStack.poll();

						if (packet != null) {
							mCmdHandler.sendMessage(mCmdHandler.obtainMessage(CMD_THREAD_MSG_SENT_REQUEST,
								packet.getCmd(), 0, null));
							//Log.d("SOC", "Request to socket: " + String.valueOf(packet.getCmd()));

							try {
								//Write
								byte[] packetBytes = packet.getPreparedPacket();
								DataOutputStream output = new DataOutputStream(mSocket.getOutputStream());
								output.writeInt(packetBytes.length);
								output.write(packetBytes, 0, packetBytes.length);

								//Read
								byte[] data = new byte[SocketDataPacket.PACKET_SIZE_BYTES_LIMIT];
								InputStream is = mSocket.getInputStream();
								//long skiped = is.skip(3);
								int read = is.read(data);
								if(read > 0) {
									byte[] trunked = new byte[read-4];

									//Skip header bytes
									System.arraycopy(data, 4, trunked, 0, read-4);
									//packet.setResponse(trunked);

									Log.d("SOC", "Response from socket: " + String.valueOf(packet.getCmd()));
									mCmdHandler.sendMessage(mCmdHandler.obtainMessage(CMD_THREAD_MSG_GOT_RESPONSE,
										packet.getCmd(), read, trunked));
								}
							} catch (SocketTimeoutException e) {
								//If had timeout exception may be connection is broken
								e.printStackTrace();
								mCmdHandler.sendMessage(mCmdHandler.obtainMessage(CMD_THREAD_MSG_GOT_ERROR,
										packet.getCmd(), 0, null));
								break;
							} catch (Exception e) {
								e.printStackTrace();
								mCmdHandler.sendMessage(mCmdHandler.obtainMessage(CMD_THREAD_MSG_GOT_ERROR,
									packet.getCmd(), 0, null));
							}
						}

						try {
							TimeUnit.MILLISECONDS.sleep(THREAD_SEND_CMD_INTERVAL);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

				//When loop is finished close the socket
				if(mSocket != null) {
					try {
						mSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				mSocket = null;
			}
		});
	}
}
