package com.persilab.bsg.lib.SocketDataProcessor;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Константин on 05.10.2014.
 */
public class SocketDataPacket {

	public static final byte CMD_GET_DEVICE_ID = 0x4C;
	public static final byte CMD_SET_DEVICE_ID = 0x6C;
	public static final byte CMD_GET_DEVICE_NAME = 0x30;
	public static final byte CMD_GET_DEVICE_CLASS = 0x31;
	public static final byte CMD_GET_DEVICE_HW = 0x32;
	public static final byte CMD_GET_DEVICE_FW = 0x33;
	public static final byte CMD_SET_PROTOCOL = 0x62;
	public static final byte CMD_SET_RESET = 0x36;
	public static final byte CMD_GET_APP_PARAMS = 0x37; //55
	public static final byte CMD_SET_APP_PARAMS = 0x38;
	public static final byte CMD_GET_DSP_PARAMS = 0x39;
	public static final byte CMD_SET_DSP_PARAMS = 0x3A;
	public static final byte CMD_GET_DATE_TIME = 0x3C;
	public static final byte CMD_SET_DATE_TIME = 0x3D;
	public static final byte CMD_GET_MAC_ADDRESS = 0x45;
	public static final byte CMD_GET_COUNTRY_CODE = 0x51;
	public static final byte CMD_SET_COUNTRY_CODE = 0x71;
	public static final byte CMD_GET_WIFI_MODE = 0x52;
	public static final byte CMD_SET_WIFI_MODE = 0x72;
	public static final byte CMD_GET_SSID = 0x53;
	public static final byte CMD_SET_SSID = 0x73;
	public static final byte CMD_GET_SECURITY_TYPE = 0x54;
	public static final byte CMD_SET_SECURITY_TYPE = 0x74;
	public static final byte CMD_GET_PASSWORD = 0x55;
	public static final byte CMD_SET_PASSWORD = 0x75;
	public static final byte CMD_GET_DEFAULT_IP_ADDRESS = 0x46;
	public static final byte CMD_SET_DEFAULT_IP_ADDRESS = 0x66;
	public static final byte CMD_GET_DEFAULT_IP_MASK = 0x47;
	public static final byte CMD_SET_DEFAULT_IP_MASK = 0x67;
	public static final byte CMD_GET_DEFAULT_IP_GATE = 0x48;
	public static final byte CMD_SET_DEFAULT_IP_GATE = 0x68;
	public static final byte CMD_GET_DEFAULT_IP_PORT = 0x49;
	public static final byte CMD_SET_DEFAULT_IP_PORT = 0x69;
	public static final byte CMD_GET_DATA = 0x3B;
	public static final byte CMD_SET_NIGHTLIGHT = 0x70;
	public static final byte RSP_ERROR = 0x0D; //0x0A

	public static final int PACKET_SIZE_BYTES_LIMIT = 64;

	private byte[] mData, mResponse;
	private byte mCmd;
	private byte bDeviceAddrLow, bDeviceAddrHigh;

	public SocketDataPacket() {}

	public SocketDataPacket(byte cmd) {
		mCmd = cmd;
	}

	public static String parseStringResponse(String data) {
		Pattern pattern = Pattern.compile("([\\w\\d.:]+)");
		Matcher matcher = pattern.matcher(data);

		if(matcher.find()) {
			try {
				return matcher.group(1);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		}

		return "";
	}

	public static byte[] truncatePacketHeader(byte[] data) {
		if(data == null || data.length <= 4) return data;

		byte[] result = new byte[data.length - 4];
		System.arraycopy(data, 4, result, 0, result.length);

		return result;
	}

	public void setDeviceAddress(short addr) {
		bDeviceAddrLow = (byte) (addr >> 8);
		bDeviceAddrHigh = (byte) addr;
	}

	public short getDeviceAddress() {
		return (short) ( ((bDeviceAddrLow&0xFF)<<8) | (bDeviceAddrHigh&0xFF) );
	}

	public void setCmd(byte cmd) {
		mCmd = cmd;
	}

	public byte getCmd() {
		return mCmd;
	}

	public void setData(byte[] data) {
		mData = data;
	}

	public byte[] getData() {
		return mData;
	}

	public byte[] getPreparedPacket() {
		int headerLength = 4;
		byte length = (byte) (headerLength + (mData == null ? 0 : mData.length));
		byte[] header = new byte[] {bDeviceAddrLow, bDeviceAddrHigh, length, mCmd};
		byte[] packet = new byte[length];

		System.arraycopy(header, 0, packet, 0, headerLength);

		if(mData != null && mData.length > 0) {
			System.arraycopy(mData, 0, packet, headerLength, mData.length);
		}

		return packet;
	}

	public void setResponse(byte[] p) {
		mResponse = p;
	}

	public byte[] getResponseBytes() {
		if(mResponse.length - 4 <= 0) return null;

		byte[] trunked = new byte[mResponse.length-4];
		System.arraycopy(mResponse, 4, trunked, 0, mResponse.length-4);
		return trunked;
	}

	public String getResponseAsString() {
		return mResponse.length > 0 ? (new String(mResponse)).substring(3) : null;
	}


	private static abstract class SuStructure {
		public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
		public static final int SIZE_OF_DOUBLE = 8;
		public static final int SIZE_OF_INT = 4;
		public static final int SIZE_OF_SHORT = 2;

		public byte[] toByteArray(double value) {
			return ByteBuffer.allocate(8).order(BYTE_ORDER)
				.putDouble(value)
				.array();
		}

		public byte[] toByteArray(int value) {
			return ByteBuffer.allocate(4).order(BYTE_ORDER)
				.putInt(value)
				.array();
		}

		public byte[] toByteArray(short value) {
			return ByteBuffer.allocate(2).order(BYTE_ORDER)
				.putShort(value)
				.array();
		}

		private ByteBuffer wrapBytes(byte[] src, int start, int length) {
			try {
				byte[] slice = new byte[length];
				System.arraycopy(src, start, slice, 0, length);

				//return ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN).put(slice);

				return ByteBuffer.wrap(slice).order(ByteOrder.LITTLE_ENDIAN);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			}

			return null;
		}

		public double getDouble(byte[] src, int start) {
			try {
				return wrapBytes(src, start, SIZE_OF_DOUBLE).getDouble();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return 0;
		}

		public double getDouble(byte[] src, int start, int additional) {
			try {
				return wrapBytes(src, start, SIZE_OF_DOUBLE + additional).getDouble();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return 0;
		}

		public int getInt(byte[] src, int start) {
			try {
				return wrapBytes(src, start, SIZE_OF_INT).getInt();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return 0;
		}

		public short getShort(byte[] src, int start) {
			try {
				return wrapBytes(src, start, SIZE_OF_SHORT).getShort();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return 0;
		}

		public byte[] mergeByteArrays(Object... arrays) {
			int resultArrayLength = 0;

			for(int i=0; i<arrays.length; i++) {
				resultArrayLength += ( (byte[])arrays[i] ).length;
			}

			byte[] resultArray = new byte[resultArrayLength];
			int filledLength = 0;

			for(int i=0; i<arrays.length; i++) {
				byte[] array = (byte[])arrays[i];
				System.arraycopy(array, 0,
					resultArray, filledLength, array.length);
				filledLength += array.length;
			}

			return resultArray;
		}
	}

	public static class SuParameters extends SuStructure {
		public boolean	bSoundEnabled; //1
		public short	nMicLevel; //2
		public short	nNightlightDurationSec; //2
		public short	nLedBlinkingPeriod; //2
		public short	nPowerOffTimeout; //2

		public static final int BYTES_EXPECTED_COUNT = 7;

		public SuParameters() {}

		public SuParameters(byte[] data) throws Exception {
			//if(data.length < BYTES_EXPECTED_COUNT) return;

			bSoundEnabled = (data[0] == 1);
			nMicLevel = ByteBuffer.wrap(new byte[] {0, data[1]}).getShort();
			nNightlightDurationSec = getShort(data, 2);
			nLedBlinkingPeriod = ByteBuffer.wrap(new byte[] {0, data[4]}).getShort();
			nPowerOffTimeout = getShort(data, 6);
		}

		public byte[] getBytes() {

			return mergeByteArrays(
				new byte[] {(byte) (bSoundEnabled ? 1 : 0)},
				toByteArray(nMicLevel),
				toByteArray(nNightlightDurationSec),
				toByteArray(nLedBlinkingPeriod),
				toByteArray(nPowerOffTimeout)
			);
		}
	}

	public static class DspParameters extends SuStructure {
		public short	SAMPLING_RATE; //2
		public short	ALARM_DELAY_ON; //2
		public short	ALARM_DELAY_OFF; //2
		public double	NOISE_THRESHOLD; //8
		public double	MOTION_THRESHOLD; //8

		public static final int BYTES_EXPECTED_COUNT = 22;

		public DspParameters() {}

		public DspParameters(byte[] data) throws Exception {
			//if(data.length < BYTES_EXPECTED_COUNT) return;

			SAMPLING_RATE = getShort(data, 0);
			ALARM_DELAY_ON = getShort(data, 2);
			ALARM_DELAY_OFF = getShort(data, 4);
			NOISE_THRESHOLD = getDouble(data, 8);
			MOTION_THRESHOLD = getDouble(data, 16);
		}

		public byte[] getBytes() {
			return mergeByteArrays(
				toByteArray(SAMPLING_RATE),
				toByteArray(ALARM_DELAY_ON),
				toByteArray(ALARM_DELAY_OFF),
				mergeByteArrays(new byte[] {0,0}, toByteArray(NOISE_THRESHOLD)),
				toByteArray(MOTION_THRESHOLD)
			);
		}
	}

	public static class RealDateTime extends SuStructure {
		public byte   dd; //1
		public byte   mm; //1
		public short  yyyy; //2
		public byte   hh; //1
		public byte   nn; //1
		public byte   ss; //1
		public byte   dow; //1

		public static final int BYTES_EXPECTED_COUNT = 8;

		public RealDateTime() {}

		public RealDateTime(Date date) {
			dd =	(byte)	date.getDate();
			mm =	(byte)	(date.getMonth()+1);
			yyyy =	(short)	(date.getYear()+1900);
			hh =	(byte)	date.getHours();
			nn =	(byte)	date.getMinutes();
			ss =	(byte)	date.getSeconds();
			dow =	(byte)	date.getDay();
		}

		public RealDateTime(byte[] data) throws Exception {
			//if(data.length < BYTES_EXPECTED_COUNT) return;

			dd = data[0];
			mm = data[1];
			yyyy = getShort(data, 2);
			hh = data[4];
			nn = data[5];
			ss = data[6];
			dow = data[7];
		}

		public byte[] getBytes() {
			return mergeByteArrays(
				new byte[] {dd},
				new byte[] {mm},
				toByteArray(yyyy),
				new byte[] {hh},
				new byte[] {nn},
				new byte[] {ss},
				new byte[] {dow}
			);
		}
	}

	public enum  WifiEncoding {
		OPEN, WEP, WPA, WPAAES, WPA2AES, WPA2TKIP, WPA2;
	}

	public enum NightlightColor {
		NIGHTLIGHT_OFF,
		NIGHTLIGHT_RED,
		NIGHTLIGHT_GREEN,
		NIGHTLIGHT_BLUE,
		NIGHTLIGHT_MAGENTA,
		NIGHTLIGHT_CYAN,
		NIGHTLIGHT_YELLOW,
		NIGHTLIGHT_WHITE
	}

	public static class SuData extends SuStructure {
		public short			nBatteryVoltage; //2
		public byte				nBatteryCapacity; //1
		public byte				nChargingStatus; //1
		public short			nTemperature; //2
		public short			nHumidity; //2
		public double			fEnergy; //8
		public short			nRespirationRate; //2
		public short			nPulseRate; //2
		public boolean			bAlarm; //1
		public boolean			bPresence; //1
		public boolean			bMotion; //1
		public boolean			bSound; //1
		public NightlightColor	nNightlightColor; //4
		public int				nAlarmDurationSec; //4
		public int				nPresenceDurationSec; //4
		public int				nMotionDurationSec; //4
		public int				nSoundDurationSec; //4
		public int				nNightlightDurationSec; //4

		public static final int BYTES_EXPECTED_COUNT = 48;

		public SuData() {}

		public SuData(byte[] data) throws Exception {

			nBatteryVoltage = getShort(data, 0);
			nBatteryCapacity = data[2];
			nChargingStatus = data[3];
			nTemperature = getShort(data, 4);
			nHumidity = getShort(data, 6);
			fEnergy = getDouble(data, 8);
			nRespirationRate = getShort(data, 16);
			nPulseRate = getShort(data, 18);
			bAlarm = (data[20] == 1);
			bPresence = (data[21] == 1);
			bMotion = (data[22] == 1);
			bSound = (data[23] == 1);

			switch (getInt(data, 24)) {
				case 0: nNightlightColor = NightlightColor.NIGHTLIGHT_OFF; break;
				case 1: nNightlightColor = NightlightColor.NIGHTLIGHT_RED; break;
				case 2: nNightlightColor = NightlightColor.NIGHTLIGHT_GREEN; break;
				case 3: nNightlightColor = NightlightColor.NIGHTLIGHT_BLUE; break;
				case 4: nNightlightColor = NightlightColor.NIGHTLIGHT_MAGENTA; break;
				case 5: nNightlightColor = NightlightColor.NIGHTLIGHT_CYAN; break;
				case 6: nNightlightColor = NightlightColor.NIGHTLIGHT_YELLOW; break;
				case 7: nNightlightColor = NightlightColor.NIGHTLIGHT_WHITE; break;
				default:
					nNightlightColor = NightlightColor.NIGHTLIGHT_OFF;
			}

			nAlarmDurationSec = getInt(data, 28);
			nPresenceDurationSec = getInt(data, 32);
			nMotionDurationSec = getInt(data, 36);
			nSoundDurationSec = getInt(data, 40);
			nNightlightDurationSec = getInt(data, 44);
		}

		public byte[] getBytes() {
			return mergeByteArrays(
				toByteArray(nBatteryVoltage),
				new byte[] {nBatteryCapacity},
				new byte[] {nChargingStatus},
				toByteArray(nTemperature),
				toByteArray(nHumidity),
				toByteArray(fEnergy),
				toByteArray(nRespirationRate),
				toByteArray(nPulseRate),
				new byte[] {(byte) (bAlarm ? 1 : 0)},
				new byte[] {(byte) (bPresence ? 1 : 0)},
				new byte[] {(byte) (bMotion ? 1 : 0)},
				new byte[] {(byte) (bSound ? 1 : 0)},
				new byte[] {(byte)nNightlightColor.ordinal()},
				toByteArray(nAlarmDurationSec),
				toByteArray(nPresenceDurationSec),
				toByteArray(nMotionDurationSec),
				toByteArray(nSoundDurationSec),
				toByteArray(nNightlightDurationSec)
			);
		}
	}

	public static class SuCmd extends SuStructure {
		public NightlightColor	nNightlightColor; //1
		public short nNightlightDurationSec; //2

		public static final int BYTES_EXPECTED_COUNT = 3;

		public SuCmd() {}

		public SuCmd(byte[] data) throws Exception {
			//if(data.length < BYTES_EXPECTED_COUNT) return;

			switch (data[0]) {
				case 0: nNightlightColor = NightlightColor.NIGHTLIGHT_OFF; break;
				case 1: nNightlightColor = NightlightColor.NIGHTLIGHT_RED; break;
				case 2: nNightlightColor = NightlightColor.NIGHTLIGHT_GREEN; break;
				case 3: nNightlightColor = NightlightColor.NIGHTLIGHT_BLUE; break;
				case 4: nNightlightColor = NightlightColor.NIGHTLIGHT_MAGENTA; break;
				case 5: nNightlightColor = NightlightColor.NIGHTLIGHT_CYAN; break;
				case 6: nNightlightColor = NightlightColor.NIGHTLIGHT_YELLOW; break;
				case 7: nNightlightColor = NightlightColor.NIGHTLIGHT_WHITE; break;
				default:
					nNightlightColor = NightlightColor.NIGHTLIGHT_OFF;
			}

			nNightlightDurationSec = getShort(data, 1);
		}

		public byte[] getBytes() {
			byte nightLight = 0;

			switch (nNightlightColor) {
				case NIGHTLIGHT_OFF: nightLight = 0; break;
				case NIGHTLIGHT_RED: nightLight = 1; break;
				case NIGHTLIGHT_GREEN: nightLight = 2; break;
				case NIGHTLIGHT_BLUE: nightLight = 3; break;
				case NIGHTLIGHT_MAGENTA: nightLight = 4; break;
				case NIGHTLIGHT_CYAN: nightLight = 5; break;
				case NIGHTLIGHT_YELLOW: nightLight = 6; break;
				case NIGHTLIGHT_WHITE: nightLight = 7; break;
			}

			return mergeByteArrays(
				new byte[] {nightLight},
				toByteArray(nNightlightDurationSec)
			);
		}
	}
}
